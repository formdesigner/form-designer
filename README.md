# cscc-form-designer

Last updated: 9/5/2016


```git clone```

```npm install```

```gulp webpack-dev-server```

open browser and navigate to
```http://0.0.0.0:4000/```


#Tooling

Gulp + Webpack Dev Server + Hot Module Reloading (HMR). 

###Gulp 
http://gulpjs.com/

###Webpack
https://webpack.github.io/docs/

Webpack is pretty darn complicated with all its multitude of configuration options. Currently it's set up with support for loading .js, .jsx, .css, and image files, which is pretty much all that's needed as of now. Webpack dev server is the development server (default port 4000) that will run locally when running this application. 

###Hot Module Reloading
Replaces modules LIVE while preserving compoent state without doing a full page reload. Very, very neat. Right now it doesn't work when Main.js or Stores are modified. You will need to add some extra code in those modules to make it work. 

to boot up server and start making changes, run in terminal

```gulp webpack-dev-server```

Now you can just use your favorite text editor and make changes.


#NOTES

This application is far from complete. Make changes as you see necessary.

###Form Model

The Form Model can be found in the Java backend. The front end implementation is slightly different but is based off the backend Java classes and instance variables. Review the backend to fully understand the Form Model. 

Front-end form model can be found in ```FormEntityStore.js```

###CSS
There's a mix of inline styles and CSS. Inline styles are used for convenience and is nice to use for styles unique to certain components. CSS is found in main.css (mostly for classes) and the Blueprint CSS Framework. The grid layout is based on the Blueprint CSS Framework. There's also some other css files to use with external modules.

###ReactResizable
A library that was found on NPM, modified slightly to work with FormDesigner. More specifically, being able to set the height and width via passing props.

###ReactDnD
https://github.com/gaearon/react-dnd
React Drag and Drop is extensively used in this application. Get familiar with the API and how it works. 

###ES6 Syntax
This code base currently has a mix of ES5 and ES6 syntax. This is primarily due to the previous developer discovering ES6 and implementing it in certain areas. ES6 has loads of nice features and most modern web development tools / modules will be written in ES6. Although the browser does not currently support it (only supports ES5), everyone (including us!) uses the Babel transpiler to make it compatible with the browser. 

###Flux
https://facebook.github.io/flux/docs/overview.html
This application currently uses the basic Flux architecture to organize the data flow. There are implementations of the Flux architecture out there, the most popular being Redux. In this application, there are two stores, one for form data and the other for application state. 

###Immutable.js
https://facebook.agithub.io/immutable-js/
This library enforces data to be immutable, which is a great feature to have to reduce complexity and bugs within the application. Immutability is also awesome for optimization using shouldComponentUpdate (read below). The syntax of manipulating data with this library is indeed more cumbersome, but the gains are worth it. Documentation is really great for this library. Most of the data that you'll see in this application are Immutable, with the exception of some limited scoped data (got lazy and just need to do some quick comparisons, but it's ok). Try to avoid using the .toJS() function. You may be tempted to change Immutable data to regular data for simpler syntax or ease of writing code, but the toJS() function is expensive and will defeat a lot of the under-the-hood data copying optimizations of Immutable.

###shouldComponentUpdate()
The shouldComponentUpdate method of the React lifecycle holds much of the needed optimization that this application requires. This method will help short circuit virtual DOM comparisons, boosting performance. So far, this application doesn't leverage this method in many of its components. This is a must-have before release.

### Perf
https://facebook.github.io/react/docs/perf.html Tools to measure speed and identify performance bottlenecks.

###APIs
Currently not connected to any CSCC backend services. Will need to do this when ready. 

##TODO:
- Make ```<ValidatorConditions/>``` and ```<CompositeConditions/>``` in ```DependencyBox.jsx``` be Drag and Drop
- ```ValidatorConditionInputChooser.jsx```, choose Dependent Input via same mechanism used for choosing Associated Regions for HighlightImageBlocks
