var AppDispatcher = require('../dispatcher/Dispatcher.js');
var EventEmitter = require('events').EventEmitter;
var Immutable = require('immutable');
var _ = require('underscore');

var HelperFunctions = require('../HelperFunctions.jsx')

var AppStateStore = require('./AppStateStore.jsx');

var FormDesignerFluxConstants = require('../constants/FormDesignerFluxConstants');
var Constants = require('../constants/Constants.js');
var EntityTypes = Constants.ENTITY_TYPES;


// Initial values of state

/*
	MODEL

	FormSection
	FormEntity
		*id (integer)
		*width (integer)
		*prepend (integer)
		*append (integer)
		*entityType (string)

		FormInput
				*promptPreWidth (integer)
				*promptPreText (string)
				*promptPostWidth (integer)
				*promptPostText (string)
			
			TextInput
			
			TextareaInput
				*rows (integer)
			
			CheckboxInput
				*defaultChecked (boolean) 
			
			SelectInput
				*selectionOptions ([{label: (string), value: (string)}])
		
			DateTimeInput
				*precisionOptions ([{precision: boolean}])
				*allDateComponentsRequired (boolean)
				*displayTimeZoneOptions (boolean)
				*timeZone (Constants.TIME_ZONE)

		TextBlock
			*content (string)
			*backgroundColor (Constants.BACKGROUND_COLORS)
			*qxqTextBlock (boolean)
			*qxqHeight (integer)

		ImageBlock
			*title (string)
			*alt (string)
			*imgUrl (string)
			*aspectRatio (double)

		HighlightImageBlock
			*title (string)
			*alt (string)
			*imgUrl (string)
			*aspectRatio (double)
			*regions ([{vertices: [{x: (integer), y: (integer)}], 
						color: (string),
						name: (string)}])
	Prompt


	Validators: [
					*properties: {name: (string), value: (string)},
					*validState: boolean,
					*strong: boolean,
					*nullIsValid: boolean,
					*customFailureMessages: [{
											message: (string),
											language: (set of languages), // will have to get remotely
											country: (set of countries) // will have to get remotely
										}]]

	Dependencies: [{operator: Operators.AND
					conditions: [AppliedValidator || CompositeCondition]
					}]
					*** This is the default dependency tree for a Form Input. It is just one CompositeCondition.

			AppliedValidator is basically a Validator defined above, but has no idea of customFailureMessages or the 'strong' field
			CompositeCondition: {operator: Constants.Operators, conditions: [AppliedValidator || CompositeCondition]}
			A dependency for an Form Input can be a deeply nested tree.
*/

// Form Entities initialized with only a Form Section
// Module scoped variables

var filler = new Immutable.Map({filler: true, height: 1, entityType: EntityTypes.FILLER});
let id = 0;

var formEntities = Immutable.List.of(
	new Immutable.Map(
		{
			entityType: EntityTypes.FORM_SECTION, 
			id: getNewId(), 
			width: 24, 
			prepend: 0,
			append: 0,
			last: true,
			root: true,
			name: 'Tab',
			childEntities: Immutable.List.of(filler.set('id', getNewId()).set('width', Constants.MAX_ENTITIES_PER_ROW))
		}
	)	
);


function getFormEntity(id) {
	let formEntityPath = HelperFunctions.getFormEntityPathToId(id, formEntities);
	if (formEntityPath.size > 0) {
		return formEntities.getIn(formEntityPath);
	}
	return -1;
}

function getFormEntityIdsByEntityType(entityTypes) {
	let formInputIds = Immutable.fromJS([]);
	let formEntityQueue = formEntities;

	while (formEntityQueue.size > 0) {
		let childEntity = formEntityQueue.get(0);
		formEntityQueue = formEntityQueue.shift();
		
		const entityType = childEntity.get('entityType');
		if (entityTypes.includes(entityType)) {
			formInputIds = formInputIds.push(childEntity.get('id'));
		} else if (entityType === EntityTypes.FORM_SECTION) {
			childEntity.get('childEntities').map((childEntity) => formEntityQueue = formEntityQueue.unshift(childEntity));
		}
		
	}
	return formInputIds;
}

// @param id: ID for entity being added to be an association to a region
function toggleAssociation(id) {
	let appState = AppStateStore.getAppState();
	if (appState.get('addingAssociations')) {
		let entityId = appState.get('selectedEntityId');
		let regionId = appState.get('selectedRegionId');
		let associations = getFormEntity(entityId).get('regions').get(regionId).get('associations');
		if (associations.includes(id)) {
			//associations = associations.delete(associations.findIndex(id));
			associations = associations.filter((v) => v !== id);
		} else {
			associations = associations.push(id);
		}
		updateFormEntity(entityId, {regions: getFormEntity(entityId).get('regions').setIn([regionId, 'associations'], associations)});
	}
}

function getNewId() {
	id++;
	return id;
}

function updateRow(row) {
	let formEntityPath = HelperFunctions.getFormEntityPathToId(row.first().get('id'), formEntities);
	let id = formEntityPath.last();
	let rowIndex = 0;

	while (!formEntities.getIn(formEntityPath.pop().push(id)).get('last')) {
		formEntities = formEntities.setIn(formEntityPath.pop().push(id++), row.get(rowIndex++));
	}
	formEntities = formEntities.setIn(formEntityPath.pop().push(id), row.get(rowIndex));
}

function updateFormSection(id, childEntities) {
	var formEntityPath = HelperFunctions.getFormEntityPathToId(id, formEntities);
	//console.log(formEntities.toJS())
	formEntities = formEntities.setIn(formEntityPath.push('childEntities'), childEntities);
}

function updateFormEntities(data) {
	formEntities = data;
}

// Adding a tab, a root level form section
function addTab() {
	formEntities = formEntities.push(new Immutable.Map(
		{
			entityType: EntityTypes.FORM_SECTION,
			id: getNewId(),
			width: 24, 
			prepend: 0,
			append: 0,
			last: true,
			root: true,
			name: 'Tab' + (formEntities.size + 1),
			childEntities: Immutable.List.of(filler.set('id', getNewId()).set('width', Constants.MAX_ENTITIES_PER_ROW))
		}
	));
}

function reorderTabs(dragIndex, hoverIndex) {
	const draggedOption = formEntities.get(dragIndex);
	formEntities = formEntities.delete(dragIndex).insert(hoverIndex, draggedOption);
}

function updateFormEntity(id, data) {
	var formEntityPath = HelperFunctions.getFormEntityPathToId(id, formEntities);
	if (formEntityPath.size > 0) {
		formEntities = formEntities.mergeIn(formEntityPath, data);
	}
}

function updateFormEntityProperty(entityId, keyPath, value) {
	var formEntityPath = HelperFunctions.getFormEntityPathToId(entityId, formEntities);
	formEntities = formEntities.setIn(formEntityPath.concat(keyPath), value);
}

function removeDependenciesWithId(entityId) {
	let formEntityQueue = formEntities;

	while (formEntityQueue.size > 0) {
		let childEntity = formEntityQueue.get(0);
		formEntityQueue = formEntityQueue.shift();
		
		if (childEntity.get('dependencies')) {
			let dependencyQueue = childEntity.get('dependencies').map((dependency) => dependency.set('keyPath', ['dependencies', 0]));
			while (dependencyQueue.size > 0) {
				let dependency = dependencyQueue.get(0);
				dependencyQueue = dependencyQueue.shift();

				// If CompositeCondition
				if (dependency.get('operator')) {
					dependency.get('conditions').map(
						(dep, index) => {
							dep = dep.set('keyPath', dependency.get('keyPath').concat(['conditions', index]));
							dependencyQueue = dependencyQueue.unshift(dep);
						});
				} else if (dependency.get('dependentInputId') === entityId) {
					let formEntityPath = HelperFunctions.getFormEntityPathToId(childEntity.get('id'), formEntities);
					formEntities = formEntities.setIn(formEntityPath.concat(dependency.get('keyPath')).push('deleted'), true);
				}
			}
		} else if (childEntity.get('entityType') === EntityTypes.FORM_SECTION) {
			childEntity.get('childEntities').map((childEntity) => formEntityQueue = formEntityQueue.unshift(childEntity));
		}
	}
}

function removeFormEntity(id) {
	var formEntityPath = HelperFunctions.getFormEntityPathToId(id, formEntities);
	//console.log(formEntityPath.toJS())
	var widthOfParent = formEntities.getIn(formEntityPath.pop().pop()).get('width');
	var fillerGotAdded = false;

	if (formEntities.getIn(formEntityPath).get('last') 
			&&	(formEntityPath.last()-1 < 0 // Single Form Entity in row, in the first row
				|| formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()-1)).get('last') 
				|| formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()-1)).get('filler'))) {
			// Remove FormEntity Case 1: Only FormEntity in pop()
			formEntities = formEntities.updateIn(formEntityPath.pop(), 
				list => list.delete(formEntityPath.last()));
			formEntities = formEntities.updateIn(formEntityPath.pop(),
				list => list.insert(formEntityPath.last(), filler.set('id', -1).set('width', widthOfParent)));
			fillerGotAdded = true;
	} else if (formEntities.getIn(formEntityPath).get('last')) {
		// Remove FormEntity Case 2: Last (and not the only) FormEntity
		formEntities = formEntities.setIn(formEntityPath.pop().push(formEntityPath.last()-1).push('append'), 
			formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()-1).push('append')) 
			+ formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()).push('prepend'))
			+ formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()).push('append'))
			+ formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()).push('width'))
			+ (formEntities.getIn(formEntityPath.push('entityType')) === 'TextInput' ? 
					(formEntities.getIn(formEntityPath.push('promptPreWidth'))
					+ formEntities.getIn(formEntityPath.push('promptPostWidth'))) : 0));
		formEntities = formEntities.setIn(formEntityPath.pop().push(formEntityPath.last()-1).push('last'), true);
	} else if (!formEntities.getIn(formEntityPath).get('last')) {
		// Remove FormEntity Case 3: General Case
		formEntities = formEntities.setIn(formEntityPath.pop().push(formEntityPath.last()+1).push('prepend'), 
			formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()+1).push('prepend')) 
			+ formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()).push('prepend'))
			+ formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()).push('append'))
			+ formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()).push('width'))
			+ (formEntities.getIn(formEntityPath.push('entityType')) === 'TextInput' ? 
					(formEntities.getIn(formEntityPath.push('promptPreWidth'))
					+ formEntities.getIn(formEntityPath.push('promptPostWidth'))) : 0));
	}
	if (!fillerGotAdded) {
		formEntities = formEntities.updateIn(formEntityPath.pop(), list => list.delete(formEntityPath.last()));
	}
	return formEntities;
}

// Enable drag for Form Entity (Section) and all parent (Form Sections)
function enableDrag(id) {
	var formEntityPath = HelperFunctions.getFormEntityPathToId(id, formEntities);
	formEntities = formEntities.setIn(formEntityPath.push('canDrag'), true);
	var parentId = formEntities.getIn(formEntityPath.pop().pop().push('id'));
	if (parentId != undefined) {
		enableDrag(parentId);
	}
}

// Disable drag for Form Entity (Section) and all parent (Form Sections)
function disableDrag(id) {
	var formEntityPath = HelperFunctions.getFormEntityPathToId(id, formEntities);
	formEntities = formEntities.setIn(formEntityPath.push('canDrag'), false);
	var parentId = formEntities.getIn(formEntityPath.pop().pop().push('id'));
	if (parentId != undefined) {
		disableDrag(parentId);
	}
}

// Iterative BFS to set Form Entity IDs in order
// Would a DFS make the application more efficient?
function sequenceFormEntityIds() {
	const formEntitiesJS = formEntities.toJS();
//	console.log(formEntitiesJS);

	var q = [];
	q.push(formEntitiesJS);

	let count = 0;
	while (q.length > 0) {
		var formEntityChildren = q.shift();
		for (var i = 0; i < formEntityChildren.length; i++) {
			formEntityChildren[i].id = count++;
			if (formEntityChildren[i].entityType === EntityTypes.FORM_SECTION) {
				q.push(formEntityChildren[i].childEntities);
			}
		}
	}
	formEntities = Immutable.fromJS(formEntitiesJS);
}

var serializer = function() {
	var entities = formEntities.toJS();
	var serializedFormEntities = serialize(entities);
	console.log(JSON.stringify(serializedFormEntities));
}

var serialize = function(formEntities) {
	var serializedFormEntities = [];
	for (var i = 0; i < formEntities.length; i++) {
		var serializedFormEntity = {
			entityType: formEntities[i].entityType,
			width: formEntities[i].width,
			append: formEntities[i].append,
			prepend: formEntities[i].prepend
		};

		console.log(serializedFormEntity)

		if (formEntities[i].entityType === EntityTypes.FORM_SECTION) {
			if (formEntities[i].root) {
				serializedFormEntity.name = formEntities[i].name;
				serializedFormEntity.root = true;
			}
			serializedFormEntity.childEntities = serialize(formEntities[i].childEntities); // recursive call 
		} else if (formEntities[i].entityType !== EntityTypes.FILLER) {
			// Form Input
			serializedFormEntity.promptPreWidth = formEntities[i].promptPreWidth;
			serializedFormEntity.promptPreText = formEntities[i].promptPreText;
			serializedFormEntity.promptPostWidth = formEntities[i].promptPostWidth;

			if (formEntities[i].entityType === EntityTypes.TEXT_INPUT) {
				// nothing
			} else if (formEntities[i].entityType === EntityTypes.TEXTAREA_INPUT) {
				serializedFormEntity.rows = formEntities[i].rows;
			} else if (formEntities[i].entityType === EntityTypes.SELECT_INPUT) {
				serializedFormEntity.selectionOptions = formEntities[i].selectionOptions;
			} else if (formEntities[i].entityType === EntityTypes.CHECKBOX_INPUT) {
				serializedFormEntity.defaultChecked = formEntities[i].defaultChecked;
			} 
		}
		serializedFormEntities.push(serializedFormEntity);
	}
	return serializedFormEntities;
}

var deserializer = function(formEntitiesJSON) {
	var entities = JSON.parse(formEntitiesJSON);
	var deserializedFormEntities = deserialize(entities, 24);
	formEntities = Immutable.fromJS(deserializedFormEntities);
	sequenceFormEntityIds();
}

var deserialize = function(formEntities, maxWidth) {
	var deserializedFormEntities = [];
	var rowWidthCounter = 0;
	for (var i = 0; i < formEntities.length; i++) {
		var deserializedFormEntity = {
			entityType: formEntities[i].entityType,
			width: formEntities[i].width,
			append: formEntities[i].append,
			prepend: formEntities[i].prepend
		}

		if (formEntities[i].entityType === EntityTypes.FILLER) {
			deserializedFormEntity.filler = true;
			delete deserializedFormEntity.append;
			delete deserializedFormEntity.prepend;
		} else if (formEntities[i].entityType === EntityTypes.FORM_SECTION) {
			if (formEntities[i].root) {
				deserializedFormEntity.name = formEntities[i].name;
				deserializedFormEntity.root = true;
			}
			deserializedFormEntity.childEntities = deserialize(formEntities[i].childEntities, deserializedFormEntity.width); // recursive call 
		} else {
			deserializedFormEntity.promptPreWidth = formEntities[i].promptPreWidth;
			deserializedFormEntity.promptPreText = formEntities[i].promptPreText;
			deserializedFormEntity.promptPostWidth = formEntities[i].promptPostWidth;
			deserializedFormEntity.promptPostText = formEntities[i].promptPostText;
			rowWidthCounter += formEntities[i].promptPreWidth + formEntities[i].promptPostWidth;

			if (formEntities[i].entityType === EntityTypes.TEXT_INPUT) {
				// nothing
			} else if (formEntities[i].entityType === EntityTypes.TEXTAREA_INPUT) {
				deserializedFormEntity.rows = formEntities[i].rows;
			} else if (formEntities[i].entityType === EntityTypes.SELECT_INPUT) {
				deserializedFormEntity.selectionOptions = formEntities[i].selectionOptions;
			} else if (formEntities[i].entityType === EntityTypes.CHECKBOX_INPUT) {
				deserializedFormEntity.defaultChecked = formEntities[i].defaultChecked;
			}
		} 

		rowWidthCounter += formEntities[i].width
						+ formEntities[i].prepend
						+ formEntities[i].append;
		
		if (rowWidthCounter == maxWidth) {
			deserializedFormEntity.last = true;
			rowWidthCounter = 0;
		}
		deserializedFormEntities.push(deserializedFormEntity);
	}
	return deserializedFormEntities;
}

var FormEntityStore = _.extend({}, EventEmitter.prototype, {
	getFormEntities: function() {
		return formEntities;
	},

	getFormEntityIdsByEntityType: getFormEntityIdsByEntityType,

	emitChange: function() {
		this.emit('change');
	},

	addChangeListener: function(callback) {
		this.on('change', callback);
	},

	removeChangeListener: function(callback) {
		this.removeListener('change', callback);
	},

	getNewId: getNewId,

	getFormEntity: getFormEntity
});

AppDispatcher.register(function(payload){
	var action = payload.action;

	if (payload.source == 'SERIALIZATION') {
		switch (action.actionType) {
			case 'SERIALIZE':
				console.log(serializer())
				break;
			case 'DESERIALIZE':
				deserializer(action.json);
				FormEntityStore.emitChange();
				break;
			default:
		}
		return true;
	}


	if (payload.source != 'MODIFY_LAYOUT') {return;}

	switch (action.actionType) {
		case FormDesignerFluxConstants.FORM_ENTITY_DROP:
			dropFormEntity(action.id, action.width, action.height, action.paddingId, action.paddingType, 
				action.newFormPre, action.newFormPost, action.entityType);
			break;
		case FormDesignerFluxConstants.FORM_ENTITY_REMOVE:
			removeFormEntity(action.id);
			sequenceFormEntityIds();
			break;
		case FormDesignerFluxConstants.FORM_ENTITY_ENABLE_DRAG:
			enableDrag(action.id);
			break;
		case FormDesignerFluxConstants.FORM_ENTITY_DISABLE_DRAG:
			disableDrag(action.id);
			break;
		case FormDesignerFluxConstants.FORM_UPDATE_ROW: // from a resize
			updateRow(action.row);
			break;
		case FormDesignerFluxConstants.FORM_UPDATE_FORM_SECTION:
			updateFormSection(action.id, action.childEntities);
			break;
		case FormDesignerFluxConstants.FORM_ENTITY_UPDATE:
			updateFormEntity(action.id, action.data);
			break;
		case FormDesignerFluxConstants.FORM_ADD_TAB:
			addTab();
			break;
		case FormDesignerFluxConstants.FORM_REORDER_TABS:
			reorderTabs(action.dragIndex, action.hoverIndex);
			break;
		case FormDesignerFluxConstants.FORM_DESIGNER_UPDATE:
			updateFormEntities(action.data);
			break;
		case FormDesignerFluxConstants.FORM_HIGHLIGHT_REGION_SELECT:
			updateFormEntities(action.data);
			break;
		case FormDesignerFluxConstants.FORM_HIGHLIGHT_REGION_TOGGLE_VISIBILITY:
			updateFormEntities(action.data);
			break;
		case 'TOGGLE_ASSOCIATION':
			toggleAssociation(action.id);
			break;
		case 'UPDATE_FORM_ENTITY_PROPERTY':
			updateFormEntityProperty(action.entityId, action.keyPath, action.value);
			break;
		case 'REMOVE_DEPENDENCIES':
			removeDependenciesWithId(action.entityId);
			break;
		default:
			return true;
	}
	FormEntityStore.emitChange();

	return true;
});

module.exports = FormEntityStore;