const React = require('react');
const Immutable = require('immutable');
const Collapse = require('rc-collapse');
const Panel = Collapse.Panel;

const FormEntityStore = require('../../stores/FormEntityStore.js');

const FormDesignerActions = require('../../actions/FormDesignerActions.js');

const Constants = require('../../constants/Constants.js');
const EntityTypes = Constants.ENTITY_TYPES;

const ValidatorPanel = require('../validator/ValidatorPanel.jsx').ValidatorPanel;
const ValidatorConditionInputChooser = require('./ValidatorConditionInputChooser.jsx').ValidatorConditionInputChooser;
const ValidatorEditor = require('../validator/ValidatorEditor.jsx').ValidatorEditor;

const ValidatorHelperFunctions = require('../validator/ValidatorHelperFunctions.js');

const ValidatorCondition = React.createClass({
	getInitialState: function() {
		return {
			dependentFormInputs: this.getDependentFormInputs()
		}
	},

	componentDidMount: function() {
		FormEntityStore.addChangeListener(this.updateFormInputs);
	},

	componentWillUnmount: function() {
		FormEntityStore.removeChangeListener(this.updateFormInputs);
	},

	updateFormInputs: function() {
		this.setState({
			dependentFormInputs: this.getDependentFormInputs()
		});
	},

	// Only accept Input types of text and datetime? 
	// Not sure of specification yet
	getDependentFormInputs: function() {
		const dependentFormInputs = FormEntityStore.getFormEntityIdsByEntityType([EntityTypes.TEXT_INPUT, EntityTypes.DATETIME_INPUT]);
		return dependentFormInputs.delete(dependentFormInputs.indexOf(this.props.entityId));
	},

	toggleAccordion: function(activeKey) {
		this.setState({
			activeKey: activeKey
		});
	},

	selectDependentInputId: function(e) {
		FormDesignerActions.updateFormEntityProperty(
			this.props.entityId,
			this.props.keyPath.push('dependentInputId'),
			parseInt(e.target.value)
		);
		FormDesignerActions.updateFormEntityProperty(
			this.props.entityId,
			this.props.keyPath.push('deleted'),
			false
		);
	},

	render: function() {

		let inputType;
		let hasChosenDependentInputId = this.props.validator.get('dependentInputId') !== -1 && !this.props.validator.get('deleted');
		if (hasChosenDependentInputId) {
			inputType = FormEntityStore.getFormEntity(this.props.validator.get('dependentInputId')).get('inputType');
		}

		let compatible = ValidatorHelperFunctions.isValidatorCompatible(inputType, this.props.validator.getIn(['properties', 'name']));
		compatible = compatible && hasChosenDependentInputId;

		return (
			<Collapse
				activeKey={this.state.activeKey}
				onChange={this.toggleAccordion}
				key='onlyPanel'>
				<Panel
					header={<ValidatorPanel
								validator={this.props.validator}
								inputType={inputType}
								deleteValidator={this.props.deleteValidator}
								compatible={compatible}
								appliedValidator={false}/>}>
					
					<ValidatorConditionInputChooser
						entityId={this.props.entityId}
						dependentInputId={this.props.validator.get('dependentInputId')}
						inputType={inputType}
						dependentFormInputsList={this.state.dependentFormInputs}
						selectDependentInputId={this.selectDependentInputId}/>
					
					{hasChosenDependentInputId && !this.props.validator.get('deleted') ? 
						<ValidatorEditor
							entityId={this.props.entityId}
							dependentInputId={this.props.validator.get('dependentInputId')}
							inputType={inputType}
							keyPath={this.props.keyPath}
							validator={this.props.validator}
							appliedValidator={false}
							compatible={compatible}/> 
					: null}
				</Panel>
			</Collapse>
		);
	}
});

module.exports = {
	ValidatorCondition: ValidatorCondition
}