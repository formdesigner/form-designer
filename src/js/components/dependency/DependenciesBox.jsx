const React = require('react');
const Radium = require('radium');
const Immutable = require('immutable');
const _ = require('underscore');
const Collapse = require('rc-collapse');
const Panel = Collapse.Panel;

const FormDesignerActions = require('../../actions/FormDesignerActions.js');

const Constants = require('../../constants/Constants.js');
const InputTypes = Constants.INPUT_TYPES;
const ValidatorTypes = Constants.VALIDATOR_TYPES;
const FocusOptions = Constants.FOCUS_OPTIONS;

const ValidatorPanel = require('../validator/ValidatorPanel.jsx').ValidatorPanel;
const ValidatorEditor = require('../validator/ValidatorEditor.jsx').ValidatorEditor;

const CompositeCondition = require('./CompositeCondition.jsx').CompositeCondition;

const Draggable = require('react-draggable');
const ResizableBox = require('react-resizable').ResizableBox;
const Portal = require('react-portal');

const keyPath = Immutable.fromJS(['dependencies', 0]);

const DependenciesBox = Radium(React.createClass({
	getInitialState: function() {
		return {
			width: 300,
			height: 300
		}
	},

	componentDidMount: function() {
		let formEntityAttributesBoxDimensions = document.getElementById('formEntityAttributesBox').getBoundingClientRect();
		this.setState({
			top: this.props.appState.get('dependenciesBoxAttributes').get('coords') ? 
				this.props.appState.get('dependenciesBoxAttributes').get('coords').get('top') : formEntityAttributesBoxDimensions.top + 50,
			left: this.props.appState.get('dependenciesBoxAttributes').get('coords') ? 
				this.props.appState.get('dependenciesBoxAttributes').get('coords').get('left') : formEntityAttributesBoxDimensions.right + 50,
			width: this.props.appState.get('dependenciesBoxAttributes').get('dimensions') ? 
				this.props.appState.get('dependenciesBoxAttributes').get('dimensions').get('width') : this.state.width,
			height: this.props.appState.get('dependenciesBoxAttributes').get('dimensions') ? 
				this.props.appState.get('dependenciesBoxAttributes').get('dimensions').get('height') : this.state.height,
			headerHeight: this.calculateHeaderHeight()
		});
		FormDesignerActions.setWindowFocus(FocusOptions.DEPENDENCIES);
	},

	componentDidUpdate: function() {
		window.setTimeout(
			() => {
				let newHeaderHeight = this.calculateHeaderHeight();
				if (newHeaderHeight !== this.state.headerHeight) {
					this.setState({
						headerHeight: newHeaderHeight
					});
				}
			}
		, 0);
		// Weird, but if setTimeout isn't used, calculateHeaderHeight() returns wrong value
		// Seems like componentDidUpdate fires before all the browser DOM manipulations are complete
	},

	calculateHeaderHeight: function() {
		return document.getElementById('dependencyBoxHeader').getBoundingClientRect().height;
	},

	renderDependencies: function() {
		const dependency = this.props.dependencies.get(0);
		return <CompositeCondition
					keyPath={keyPath}
					entityId={this.props.entityId}
					operator={dependency.get('operator')}
					conditions={dependency.get('conditions')}
					root/>
	},

	focusDependenciesBox: function(e) {
		e.stopPropagation();
		FormDesignerActions.setWindowFocus(FocusOptions.DEPENDENCIES);
	},

	setDependenciesBoxCoords: function(e, data) {
		FormDesignerActions.setWindowAttributes(FocusOptions.DEPENDENCIES, 
			'coords', 
			Immutable.fromJS({
				top: data.node.getBoundingClientRect().top,
				left: data.node.getBoundingClientRect().left
			}));
	},

	onResizeStop: function(e, dimensions) {
		window.setTimeout(() => FormDesignerActions.enableDrag(), 0);
		FormDesignerActions.setWindowAttributes(FocusOptions.DEPENDENCIES, 
			'dimensions', 
			Immutable.fromJS({
				width: dimensions.size.width,
				height: dimensions.size.height
			}));
	},


	render: function() {
		const dependencyBoxStyle = {
			top: this.state.top, 
			left: this.state.left, 
			position: 'absolute', 
			width: this.state.width, 
			height: this.state.height, 
			background: 'rgba(255,0,0,.3)', 
			border: '1px black solid',
			zIndex: this.props.appState.get('windowFocus') === FocusOptions.DEPENDENCIES ? 99999 : 'auto'
		};

		const dependencyHeaderStyle = {
			position: 'fixed', 
			top: 0,
			left: 0, 
			right: 0
		};

		const headerMessageStyle={
			background: this.props.appState.get('windowFocus') === FocusOptions.DEPENDENCIES ? 'green' : 'rgba(0,0,0,1)',
			color: 'white',
			':hover': {
				cursor: 'pointer'
			}
		};

		const entity = this.props.entity;

		const header = 
			<div
				id='dependencyBoxHeader'
				style={dependencyHeaderStyle}>
				<div
					style={headerMessageStyle}
					id={'dependencyBoxHandle'}>
						Dependencies for {entity.get('entityType') + entity.get('id')} of 
						Input Type {entity.get('inputType')}
				</div>
			</div>;

		const dependencyBodyStyle = {
			position: 'fixed',
			top: this.state.headerHeight,
			left: 0,
			right: 0,
			background: 'white',
			bottom: 20,
			overflow: 'auto'
		};

		const body = 
			<div
				style={dependencyBodyStyle}
				id='dependencyBoxBody'>
				{this.renderDependencies()}
			</div>


		const dependencyBoxFooterStyle = {
			position: 'fixed', 
			bottom: 0, 
			left: 0,
			right: 0,
			width: '100%', 
			height: '20px',
			background: 'rgb(227,220,192)'
		};

		const footer = 
			// Filler footer right now
			<div
				style={dependencyBoxFooterStyle}>
			</div>

		// setTimeout on onResizeStop is a little hack to delay enabling canDrag in appState so 
		// that the conditionals in the  'click' event in Main.js won't pass
		return (

			// Using Portal to attach on HTML body element 
			// Useful so dimensions won't affect the layout of other HTML elements
			<Portal
				isOpened>
				<Draggable
					handle='#dependencyBoxHandle'
					onMouseDown={this.focusDependenciesBox}
					onStop={this.setDependenciesBoxCoords}>
					<div
						style={dependencyBoxStyle}
						onClick={this.focusDependenciesBox}>
						<ResizableBox
							width={this.state.width}
							height={this.state.height}
							minConstraints={[0, this.state.headerHeight + 20]}
							onResize={(e, dimensions) => this.setState({width: dimensions.size.width, height: dimensions.size.height})}
							onResizeStart={(e) => FormDesignerActions.disableDrag()}
							onResizeStop={this.onResizeStop}>
							
							{header}
							{body}
							{footer}

						</ResizableBox>
					</div>
				</Draggable>
			</Portal>
		);
	}
}));

module.exports = {
	DependenciesBox: DependenciesBox
}