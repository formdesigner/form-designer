const React = require('react');
const Immutable = require('immutable');

const FormEntityStore = require('../../stores/FormEntityStore.js');
const Constants = require('../../constants/Constants.js');
const EntityTypes = Constants.ENTITY_TYPES;

const ValidatorConditionInputChooser = React.createClass({

	render: function() {
		return (
			<div>
			Input ID
				<select
					value={this.props.dependentInputId}
					onChange={this.props.selectDependentInputId}>
					{this.props.dependentFormInputsList.map(
						(entityId, index) => 
							<option
								value={entityId}
								key={index}>
								Entity  {entityId}
							</option>).unshift(<option value='-1'/>)}
				</select>
				<button
					onClick={(e) => FormDesignerActions.enableSelectDependentInputMode()}>
					Select
				</button>
				Input Type: {this.props.inputType ? this.props.inputType : 'Not Selected'}
			</div>
		);
	}
});

module.exports = {
	ValidatorConditionInputChooser: ValidatorConditionInputChooser
}