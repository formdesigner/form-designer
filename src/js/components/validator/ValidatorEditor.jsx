const React = require('react');
const Radium = require('radium');
const Immutable = require('immutable');
const _ = require('underscore');
const Collapse = require('rc-collapse');
const Panel = Collapse.Panel;

const FormDesignerActions = require('../../actions/FormDesignerActions.js');

const Constants = require('../../constants/Constants.js');
const InputTypes = Constants.INPUT_TYPES;
const ValidatorTypes = Constants.VALIDATOR_TYPES;

const EnumValidatorEditor = require('./EnumValidatorEditor.jsx').EnumValidatorEditor;
const RangeValidatorEditor = require('./RangeValidatorEditor.jsx').RangeValidatorEditor;
const SubjectInputValidatorEditor = require('./SubjectInputValidatorEditor.jsx').SubjectInputValidatorEditor;
const PatternValidatorEditor = require('./PatternValidatorEditor.jsx').PatternValidatorEditor;
const EmptyFieldValidatorEditor = require('./EmptyFieldValidatorEditor.jsx').EmptyFieldValidatorEditor;
const CustomFailureMessagesContainer = require('./CustomFailureMessagesContainer.jsx').CustomFailureMessagesContainer;

const ValidatorPropertiesCheckboxes = require('./ValidatorPropertiesCheckboxes.jsx').ValidatorPropertiesCheckboxes;

const validatorTypeMap = {
	integer: ['' , ValidatorTypes.PATTERN, ValidatorTypes.EMPTY_FIELD, ValidatorTypes.ENUM, ValidatorTypes.RANGE],
	float:['', ValidatorTypes.PATTERN, ValidatorTypes.EMPTY_FIELD, ValidatorTypes.ENUM, ValidatorTypes.RANGE],
	date: ['', ValidatorTypes.EMPTY_FIELD, ValidatorTypes.ENUM, ValidatorTypes.RANGE],
	string: ['', ValidatorTypes.PATTERN, ValidatorTypes.EMPTY_FIELD, ValidatorTypes.ENUM, ValidatorTypes.SUBJECT_VALIDATION]
};

const ValidatorEditor = React.createClass({
	propTypes: {
		// For AppliedValidators or ValidatorConditions
		appliedValidator: React.PropTypes.bool
	},

	onChangeValidatorType: function(e) {		
		let newValidatorType = e.target.value;
		let newValidatorValue;
		if (newValidatorType === ValidatorTypes.ENUM) {
			newValidatorValue = Immutable.fromJS([]);
		} else if (newValidatorType === ValidatorTypes.RANGE) {
			newValidatorValue = 
				Immutable.fromJS(
					{
						minValue: '',
						maxValue: '',
						minInclusive: false,
						maxInclusive: false
					});
		} else if (newValidatorType === ValidatorTypes.EMPTY_FIELD) {	
			newValidatorValue = false;
		} else if (newValidatorType === ValidatorTypes.PATTERN) {
			newValidatorValue = '';
		} else if (newValidatorType === ValidatorTypes.SUBJECT_VALIDATION) {
		}

		let newValidator = Immutable.fromJS(
						{
							properties: {
								name: newValidatorType,
								value: newValidatorValue
							},
							validState: true,
							strong: false,
							nullIsValid: false
						}
					);

		if (this.props.appliedValidator) {
			newValidator = newValidator.set('customFailureMessages', Immutable.fromJS([]));
		} else {
			newValidator = newValidator.set('dependentInputId', this.props.dependentInputId);
		}

		FormDesignerActions.updateFormEntityProperty(
				this.props.entityId,
				this.props.keyPath,
				newValidator);
	},

	render: function() {

		const entity = this.props.entity;
		const inputType = this.props.inputType;
		const validatorType = this.props.validator.get('properties').get('name');
		
		const validatorTypesSelectStyle = {
			verticalAlign: 'top'
		};

		let validatorTypesSelect = 
			<select
				style={validatorTypesSelectStyle}
				onChange={this.onChangeValidatorType}
				value={validatorType}>
				{_.map(validatorTypeMap[inputType], 
					(type, index) =>
						<option 
							value={type}
							key={index}>
							{type ? ValidatorTypes.DISPLAY_NAMES[type] : ''}
						</option> )}
			</select>;

		let validatorProperties;
		if (validatorType === ValidatorTypes.PATTERN) {
			validatorProperties = 
				<PatternValidatorEditor
					entityId={this.props.entityId}
					pattern={this.props.validator.get('properties').get('value')}
					keyPath={this.props.keyPath}/>
		} else if (validatorType === ValidatorTypes.EMPTY_FIELD) {
			validatorProperties = 
				<EmptyFieldValidatorEditor
					entityId={this.props.entityId}
					checked={this.props.validator.get('properties').get('value')}
					keyPath={this.props.keyPath}/>
		} else if (validatorType === ValidatorTypes.ENUM) {
			validatorProperties = 
				<EnumValidatorEditor
					entityId={this.props.entityId}
					enumValues={this.props.validator.get('properties').get('value')}
					keyPath={this.props.keyPath}/>
		} else if (validatorType === ValidatorTypes.RANGE) {
			validatorProperties = 
				<RangeValidatorEditor
					entityId={this.props.entityId}
					rangeProperties={this.props.validator.get('properties').get('value')}
					keyPath={this.props.keyPath}/>
		} else if (validatorType === ValidatorTypes.SUBJECT_VALIDATION) {
			validatorProperties = 
				<SubjectInputValidatorEditor/>
		}

		let validatorPropertiesCheckboxes;
		if (validatorType === ValidatorTypes.PATTERN 
			|| validatorType === ValidatorTypes.ENUM
			|| validatorType === ValidatorTypes.RANGE
			|| validatorType === ValidatorTypes.SUBJECT_VALIDATION) {
				validatorPropertiesCheckboxes = 
					<ValidatorPropertiesCheckboxes
						entityId={this.props.entityId}
						keyPath={this.props.keyPath}
						validState={this.props.validator.get('validState')}
						nullIsValid={this.props.validator.get('nullIsValid')}
						strong={this.props.validator.get('strong')}
						appliedValidator={this.props.appliedValidator}/>
		}

		let validatorCustomFailureMessages = 
			<CustomFailureMessagesContainer
				entityId={this.props.entityId}
				customFailureMessages={this.props.validator.get('customFailureMessages')}
				keyPath={this.props.keyPath}/>

		return (
			<div>
				{validatorTypesSelect}
				{this.props.compatible ? validatorProperties : null}
				{this.props.compatible ? validatorPropertiesCheckboxes : null}
				{this.props.compatible && this.props.appliedValidator ? validatorCustomFailureMessages : null}
			</div>
		);
	}
});

module.exports = {
	ValidatorEditor: ValidatorEditor
}