const React = require('react');

const FormDesignerActions = require('../../actions/FormDesignerActions.js');

const Languages = ['', 'English', 'Spanish'];
const Countries = {'English' : ['', 'America', 'Canada'], 'Spanish' : ['', 'America', 'Mexico']};

const CustomFailureMessageEditor = React.createClass({

	render: function() {

		const message = this.props.customFailureMessage.get('message');
		const language = this.props.customFailureMessage.get('language');
		const country = this.props.customFailureMessage.get('country');

		const customMessageInputStyle = {
			height: '75px',
			width: '100%',
			padding: '0px'
		};

		const languageSelectStyle = {
			marginTop: '0px',
			marginLeft: '4px',
			width: '100px'
		};

		const countrySelectStyle = {
			marginTop: '0px',
			marginLeft: '4px',
			width: '100px'
		};

		return (
			<div>
				<textarea
					style={customMessageInputStyle}
					value={message}
					onChange={(e) => FormDesignerActions.updateFormEntityProperty(
										this.props.entityId, 
										this.props.keyPath.push('message'), 
										e.target.value)}/>
				<div>
					<div>
						Language 
						<select
							style={languageSelectStyle}
							value={language}
							onChange={(e) => FormDesignerActions.updateFormEntityProperty(
												this.props.entityId, 
												this.props.keyPath, 
												this.props.customFailureMessage.merge({language: e.target.value, country: ''}))}>
							{Languages.map((language) =>
								<option 	
									value={language}
									key={language}> 
									{language} 
								</option>)}
						</select>
					</div>
					
					<div>
						Country
						<select
							style={countrySelectStyle}
							value={country}
							onChange={(e) => FormDesignerActions.updateFormEntityProperty(
										this.props.entityId, 
										this.props.keyPath.push('country'), 
										e.target.value)}>
							{language ? Countries[language].map((country) =>
								<option 	
									value={country}
									key={country}> 
									{country} 
								</option>) : null}
						</select>
					</div>
				</div>
			</div>
		);
	}
});

module.exports = {
	CustomFailureMessageEditor: CustomFailureMessageEditor
}