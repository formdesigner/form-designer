const React = require('react');
const Radium = require('radium');
const Immutable = require('immutable');
const _ = require('underscore');
const Collapse = require('rc-collapse');
const Panel = Collapse.Panel;

const FormDesignerActions = require('../../actions/FormDesignerActions.js');

const CustomFailureMessagePanel = require('./CustomFailureMessagePanel.jsx').CustomFailureMessagePanel;
const CustomFailureMessageEditor = require('./CustomFailureMessageEditor.jsx').CustomFailureMessageEditor;

const CustomFailureMessagesContainer = React.createClass({
	getInitialState: function() {
		return {
			containerActiveKey: undefined,
			messageActiveKey: undefined
		}
	},

	toggleContainer: function(activeKey) {
		this.setState({
			containerActiveKey: activeKey
		});
	},

	toggleMessage: function(activeKey) {
		this.setState({
			messageActiveKey: activeKey
		});
	},

	deleteCustomFailureMessage: function(index) {
		return (e) => {
			FormDesignerActions.updateFormEntityProperty(
				this.props.entityId, 
				this.props.keyPath.concat(['customFailureMessages']), 
				this.props.customFailureMessages.delete(index));
			e.stopPropagation();

			if (this.state.messageActiveKey) {
				if (index < this.state.messageActiveKey.match(/\w+/)) {
					this.setState({
						messageActiveKey: this.state.messageActiveKey.replace(/\w+/, this.state.messageActiveKey.match(/\w+/)[0] - 1)
					});
					// Using == instead of === because comparing number to 'number'
				} else if (index == this.state.messageActiveKey.match(/\w+/)) {
					this.setState({
						messageActiveKey: undefined
					});
				}
			}
		}
	},

	addCustomFailureMessage: function() {
		FormDesignerActions.updateFormEntityProperty(
			this.props.entityId,
			this.props.keyPath.push('customFailureMessages'),
			this.props.customFailureMessages.push(
				Immutable.fromJS(
								{
									message: '',
									language: '',
									country: ''
								}
							))
			);
	},

	renderCustomFailureMessages: function() {
		return this.props.customFailureMessages.map(
			(customFailureMessage, index) =>
				<Panel
					header={<CustomFailureMessagePanel
								customFailureMessage={customFailureMessage}
								deleteCustomFailureMessage={this.deleteCustomFailureMessage(index)}/>}
					key={index}>
					<CustomFailureMessageEditor
						entityId={this.props.entityId}
						keyPath={this.props.keyPath.concat(['customFailureMessages', index])}
						customFailureMessage={customFailureMessage}/>
				</Panel>
		).toJS();
	},

	render: function() {

		const addButtonStyle = {
			'backgroundImage': 'url(statics/plus-button.jpeg)', 
			'backgroundSize': 'contain', 
			'backgroundRepeat': 'no-repeat', 
			'height': '15px',
		};

		return (
			<Collapse
				activeKey={this.state.containerActiveKey}
				onChange={this.toggleContainer}
				accordion>
				<Panel
					header={this.props.customFailureMessages.size + ' Custom Failure Message(s)'}>
					<Collapse
						activeKey={this.state.messageActiveKey}
						onChange={this.toggleMessage}
						accordion>
						{this.renderCustomFailureMessages()}
					</Collapse>
					<button
						onClick={this.addCustomFailureMessage}
						style={addButtonStyle}/>
				</Panel>
			</Collapse>
		);
	},
});


module.exports = {
	CustomFailureMessagesContainer: CustomFailureMessagesContainer
}