const React = require('react');

const FormDesignerActions = require('../../actions/FormDesignerActions.js');

const EnumValueRow = React.createClass({

	editEnumValue: function(e) {
		FormDesignerActions.updateFormEntityProperty(
			this.props.entityId,
			this.props.keyPath,
			e.target.value);
	},

	deleteEnumValue: function(e) {
		this.props.deleteEnumValue(this.props.enumIndex);
	},

	render: function() {
		const enumValueRowStyle = {
			wordWrap: 'break-word',
			marginRight: '2px'
		};

		const styleDeleteButton = {
			'backgroundImage': 'url(statics/x-button.png)', 
			'backgroundSize': 'contain', 
			'backgroundRepeat': 'no-repeat', 
			'height': '15px', 
			'width': '15px'
		};

		const styleTextInput = {
			margin: '0px', 
			width: '85px'
		};

		return (
			<div
				style={enumValueRowStyle}>
				<button
					onClick={this.deleteEnumValue}
					style={styleDeleteButton}/>
				<input
					style={styleTextInput}
					type='text'
					value={this.props.value}
					onChange={this.editEnumValue}/>
			</div>
		);
	}
});



const EnumValidatorEditor = React.createClass({

	addEnumValue: function(e) {
		FormDesignerActions.updateFormEntityProperty(
			this.props.entityId,
			this.props.keyPath.concat(['properties', 'value']),
			this.props.enumValues.push(''));
	},

	deleteEnumValue: function(enumIndex) {
		FormDesignerActions.updateFormEntityProperty(
			this.props.entityId,
			this.props.keyPath.concat(['properties', 'value']),
			this.props.enumValues.delete(enumIndex));
	},

	render: function() {
		const enumValuesEditorStyle = {
			height: '75px', 
			width: '125px', 
			marginLeft: '5px',
			background: 'rgb(245,245,220)',
			display: 'inline-block',
			overflowY: 'scroll',
			overflowX: 'hidden'
		}

		const addButtonStyle = {
			'backgroundImage': 'url(statics/plus-button.jpeg)', 
			'backgroundSize': 'contain', 
			'backgroundRepeat': 'no-repeat', 
			'height': '15px',
		};

		return (
			<div
				style={enumValuesEditorStyle}>
				{this.props.enumValues.map(
					(enumValue, index) =>
						<EnumValueRow
							entityId={this.props.entityId}
							keyPath={this.props.keyPath.concat(['properties', 'value', index])}
							value={enumValue}
							deleteEnumValue={this.deleteEnumValue}
							enumIndex={index}/>
				)}
				<button
					onClick={this.addEnumValue}
					style={addButtonStyle}/>
			</div>
		);
	}
});

module.exports = {
	EnumValidatorEditor: EnumValidatorEditor
}