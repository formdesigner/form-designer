var React = require('react');
var Modal = require('react-modal');
var $ = require('jquery');
var Immutable = require('Immutable');
var _ = require('underscore');

var FormDesignerActions = require('../actions/FormDesignerActions.js');

var ColorPicker = require("react-simple-colorpicker");

var BlueprintWidths = require('../constants/blueprint-widths.js');
var Constants = require('../constants/Constants.js');

const HighlightRegionsAttributes = React.createClass({
	getInitialState: function() {
		return ({
			adding: false,
			canFinishDraw: false,
			currentColor: 'rgba(0,0,0,1)'
		})
	},

	startAddingRegion: function() {
		this.setState({
			adding: true, 
			canFinishDraw: false
		});

		this.canvas = null;
		this.context = null;
		this.currentVertices = [];
		this.currentOrigin = {};
		this.currentlyDrawing = false;
		this.prevCanvasState = null;
	},

	componentDidUpdate: function() {
		if (this.state.adding && this.state.canFinishDraw) {
			this.initializeCanvas();
		}
	},

	initializeCanvas: function() {
		this.canvas = document.getElementById('drawRegionCanvas');
		this.context = this.canvas.getContext('2d');
		if (this.state.canFinishDraw) {
			this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);

			let vertices = this.currentVertices;

			this.context.beginPath();
			this.context.moveTo(vertices[0].x, vertices[0].y);
			let counter = 1;
			while (counter < vertices.length) {
				this.context.lineTo(vertices[counter].x, vertices[counter].y);
				counter++;
			}
			this.context.closePath();
			this.context.fillStyle = this.state.currentColor;
			this.context.fill();
		}
	},

	onMouseDownCanvas: function(e) {
		let offsetX = e.clientX - e.currentTarget.getBoundingClientRect().left;
		let offsetY = e.clientY - e.currentTarget.getBoundingClientRect().top;
		
		// First point
		if (this.currentOrigin.x === undefined && this.currentOrigin.y === undefined) {
			this.currentlyDrawing = true;
			this.currentOrigin.x = offsetX;
			this.currentOrigin.y = offsetY;
			this.context.arc(offsetX, offsetY, 1, 0, Math.PI * 2);
			this.context.fill();
			this.context.beginPath();
			this.context.moveTo(offsetX, offsetY);

			this.prevCanvasState = this.context.getImageData(0, 0, this.canvas.width, this.canvas.height);
			this.currentVertices.push({x: offsetX, y: offsetY});

		} else if (this.currentlyDrawing) {
			this.context.beginPath();
			this.context.moveTo(this.currentVertices[this.currentVertices.length-1].x, this.currentVertices[this.currentVertices.length-1].y);
			this.context.lineTo(offsetX, offsetY);
			this.context.stroke();
			this.context.closePath();

			this.prevCanvasState = this.context.getImageData(0, 0, this.canvas.width, this.canvas.height);
			this.currentVertices.push({x: offsetX, y: offsetY});

			if ((offsetX <= this.currentOrigin.x + 5 && offsetX >= this.currentOrigin.x - 5)
				&& (offsetY  <= this.currentOrigin.y + 5 && offsetY >= this.currentOrigin.y - 5)) {
				this.currentlyDrawing = false;
				this.context.closePath();
				this.finishDraw();
			}
		}
	},

	// Draw the highlight line preview
	onMouseMoveCanvas: function(e) {
		let offsetX = e.clientX - e.currentTarget.getBoundingClientRect().left;
		let offsetY = e.clientY - e.currentTarget.getBoundingClientRect().top;

		if (this.currentlyDrawing) {
			this.context.putImageData(this.prevCanvasState, 0, 0);
			this.context.beginPath();
			this.context.moveTo(this.currentVertices[this.currentVertices.length-1].x, this.currentVertices[this.currentVertices.length-1].y);
			this.context.lineTo(offsetX, offsetY);
			this.context.stroke();
		}
	},

	finishDraw: function() {
		this.setState({canFinishDraw: true});
	},

	changeColor: function(color) {
		this.setState({currentColor: color});
		this.forceUpdate();
	},

	addRegion: function() {
		let newRegion = Immutable.fromJS({
			vertices: this.currentVertices,
			color: this.state.currentColor,
			name: 'Region ' + (this.props.regions.size + 1),
			associations: []
		});

		FormDesignerActions.updateFormEntity(this.props.id, 
			{
				regions: this.props.regions === undefined ?  
					Immutable.fromJS([newRegion]) 
						: this.props.regions.push(newRegion)
			}
		);

		this.resetState();
	},

	resetState: function() {

		this.setState(
			{
				adding: false,
				canFinishDraw: false,
				currentColor: 'rgba(0,0,0,1)'
			}
		);

		this.canvas = null;
		this.context = null;
		this.currentVertices = [];
		this.currentOrigin = {};
		this.currentlyDrawing = false;
		this.prevCanvasState = null;
	},

	deleteRegion: function(e) {
		e.stopPropagation();
		FormDesignerActions.updateFormEntity(this.props.id, 
			{
				regions: this.props.regions.delete($('.region').index(e.currentTarget.parentElement))
			}
		);
	},

	toggleRegionVisibility: function(e) {
		e.stopPropagation();
		let regionIndex = $('.region').index(e.currentTarget.parentElement);
		let isHiding = this.props.hiddenRegions.toJS().indexOf(regionIndex) === -1;
		
		FormDesignerActions.updateFormEntity(this.props.id, 
			{
				hiddenRegions: isHiding ? 
									this.props.hiddenRegions.push(regionIndex) 
										: Immutable.fromJS((_.without(this.props.hiddenRegions.toJS(), regionIndex)))
			}
		);

		if (!isHiding) {
			FormDesignerActions.setSelectedRegion(regionIndex);
		} else if (regionIndex === this.props.appState.get('selectedRegionId')) {
			FormDesignerActions.setSelectedRegion(-1);
		}
	},

	// Click Region to 'focus'
	selectRegion: function(e) {
		e.stopPropagation();
		let regionIndex = $('.region').index($(e.currentTarget).parent());

		FormDesignerActions.setSelectedRegion(regionIndex);
		FormDesignerActions.setSelectedEntity(this.props.id);
	},

	changeRegionName: function(e) {
		let regionIndex = $('.region').index($(e.currentTarget).parent());

		FormDesignerActions.updateFormEntity(this.props.id, 
			{
				regions: this.props.regions.setIn([regionIndex, 'name'], e.target.value)
			}
		);
	},

	startAddingAssociations: function() {
		FormDesignerActions.enableAddingAssociationsMode();
	},

	stopAddingAssociations: function() {
		FormDesignerActions.disableAddingAssociationsMode();
	},

	render: function() {

		const modalStyle = {
			overlay: {
				zIndex: 2
			},

			content: {
				top: '50%',
				bottom: 'auto',
				left: '50%',
				right: 'auto',
				marginTop: (BlueprintWidths[this.props.width] / this.props.aspectRatio) / -2,
				marginLeft: BlueprintWidths[this.props.width] / -2
			}
		}

		return (
			<div>
				<div
					style={{
						float: 'left'
					}}>
					Regions
					<div
						id='regionList'
						style={{
							border: '1px solid black'
						}}>
						{
							this.props.regions ? 
								(this.props.regions.toJS().map(
									function(region, index) {

										// toggle-able, changes backgroundImage
										// more vars in global state?
										const styleRegionVisibilityButton = {
											'backgroundImage': this.props.hiddenRegions.toJS().indexOf(index) === -1 ? 'url(statics/openeye.png)' : 'url(statics/closedeye.png', 
											'backgroundSize': 'contain', 
											'backgroundRepeat': 'no-repeat', 
											'height': '15px',
											'width': '15px'
										};

										const styleDeleteRegionButton = {
											'backgroundImage': 'url(statics/x-button.png)', 
											'backgroundSize': 'contain', 
											'backgroundRepeat': 'no-repeat', 
											'height': '15px',
											'width': '15px'
										};

										const styleRegionInput = {
											'background': index === this.props.appState.get('selectedRegionId') ? 'green' : ''
										};

										return (
											<div
												className='region' 
												key={index}> 
												<button
													style={styleRegionVisibilityButton}
													onClick={this.toggleRegionVisibility}/>
												<input 
													style={styleRegionInput}
													value={region.name}
													onClick={this.selectRegion}
													onChange={this.changeRegionName}/>
												<button
													style={styleDeleteRegionButton}
													onClick={this.deleteRegion}/>
											</div>
										);
									}.bind(this))
							): <div> No Regions Added </div>
						}
					</div>

					<button
						onClick={this.startAddingRegion}>
						Add Region
					</button>
				</div>

				{this.props.appState.get('selectedRegionId')  > -1 ? 
					(<div
						style={{
							float: 'left'
						}}>
						Associations

						<div
							style={{
								border: '1px solid black'
							}}>

							{this.props.regions.toJS()[this.props.appState.get('selectedRegionId')].associations.length > 0 ?
								this.props.regions.toJS()[this.props.appState.get('selectedRegionId')].associations.map(
								(association, index) => {
									return (
										<div
											key={index}>
											{'Entity ' + association}
										</div>
									);
								}
							) : <div>None</div>}
						</div>

						<button
							style={{display:'block'}}
							onClick={this.startAddingAssociations}>
							Add Association
						</button>

						{
							this.props.appState.get('addingAssociations') ? 
							(<button
								onClick={this.stopAddingAssociations}>
								Done 
							</button>) : null
						}

					</div>) : null}


				<Modal
					isOpen={this.state.adding}
					style={modalStyle}
					shouldCloseOnOverlayClick={false}
					onAfterOpen={this.initializeCanvas}>
					<div
						style={{position: 'relative'}}
						onClick={(e)=> e.stopPropagation()}>
						<img 
							src={this.props.imgUrl}
							style={{width: BlueprintWidths[this.props.width]}}/>
						<canvas
							id='drawRegionCanvas'
							style={{position: 'absolute',
									top:0, 
									left:0}}
							width={BlueprintWidths[this.props.width]}
							height={BlueprintWidths[this.props.width] / this.props.aspectRatio}
							onMouseDown={this.onMouseDownCanvas}
							onMouseMove={this.onMouseMoveCanvas}/>
						<button 
							disabled={!this.state.canFinishDraw} 
							onClick={this.addRegion}> 
								Done 
						</button>
						<button
							onClick={this.resetState}> 
								Exit 
						</button>

					<div 
						className={'colorpicker'}
						style={{visibility: this.state.canFinishDraw ? 'inherit' : 'hidden'}}>
							<ColorPicker 
								color={this.state.currentColor} 
								onChange={this.changeColor}/>
						</div>
					</div>
				</Modal>
			</div>
		);
	}
});

module.exports = {
	HighlightRegionsAttributes : HighlightRegionsAttributes
}