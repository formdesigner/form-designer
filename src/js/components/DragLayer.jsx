var React = require('react');
var ReactDOM = require('react-dom');
var DragLayer = require('react-dnd').DragLayer;

var TextAreaAutoResize = require('./formentities/TextAreaAutoResize.jsx').TextAreaAutoResize;
var SelectPortal = require('./formentities/SelectDropdown.jsx').SelectPortal;
var Checkbox = require('./formentities/Checkbox.jsx').Checkbox;
var TextAreaResizable = require('./formentities/TextAreaResizable.jsx').TextAreaResizable;
var DateTime = require('./formentities/DateTime.jsx').DateTime;
var TextBlock = require('./formentities/TextBlock.jsx').TextBlock;

var BlueprintWidths = require('../constants/blueprint-widths.js');
var Constants = require('../constants/Constants.js');
var EntityTypes = Constants.ENTITY_TYPES;

function collect(monitor) {
	return {
		item: monitor.getItem(),
		itemType: monitor.getItemType(),
		currentOffset: monitor.getClientOffset(),
		isDragging: monitor.isDragging(),
	};
}

// Primary use of DragLayer is to position the top left corner of FormEntity under cursor upon Dragging
var CustomDragLayer = React.createClass({
	getInitialState: function() {
		return {
			dragPreview: null
		}
	},      
	
	generateDragPreviewChildren: function(formEntity, i) {
		return (
			<div 
				className={'prepend-' + formEntity.prepend + ' append-' + formEntity.append}
				key={i}>
				{this.generateDragPreview(formEntity)}
			</div>
		);
	},

	generateFormInputPreview: function(formEntity) {
		let className;
		let bodyPreview;
		if (formEntity.entityType === EntityTypes.TEXT_INPUT) {
			className = 'textInput';
			bodyPreview = <TextAreaAutoResize/>
		} else if (formEntity.entityType === EntityTypes.TEXTAREA_INPUT) {
			className = 'textareaInput';
			bodyPreview = <TextAreaResizable
							text={formEntity.text}
							width={formEntity.width}
							rows={formEntity.rows}
							rowHeight={13}/>
		} else if (formEntity.entityType === EntityTypes.SELECT_INPUT) {
			className = 'selectInput';
			bodyPreview = <SelectPortal
							selectionOptions={formEntity.selectionOptions}/>
		} else if (formEntity.entityType === EntityTypes.CHECKBOX_INPUT) {
			className = 'checkboxInput';
			bodyPreview = <Checkbox 
							defaultChecked={formEntity.defaultChecked}
							readOnly/>;
		} else if (formEntity.entityType === EntityTypes.DATETIME_INPUT) {
			className = 'dateTimeInput';
			bodyPreview = <DateTime/>
		}

		return (
			 <div 
				className={'span-' + (formEntity.width + formEntity.promptPreWidth + formEntity.promptPostWidth)}
				style={{background: 'rgba(204,158,228,.5)'}}>
				<div
					className={formEntity.promptPreWidth > 0 ? ('prompt ' + 'span-' + formEntity.promptPreWidth) : ''}
					style={{float: 'left'}}>
					<TextAreaAutoResize text={formEntity.promptPreText}/>
				</div>
				<div
					className={className + ' span-' + formEntity.width}>
					{bodyPreview}
				</div>
				<div
					className={'prompt ' + 'span-' + formEntity.promptPostWidth}
					style={{marginRight: 0}}>
					<TextAreaAutoResize 
						text={formEntity.promptPostText}/>
				</div>
			</div>
		);
	},

	// Can you just render a 'dummy' component?
	generateDragPreview: function(formEntity) {
		switch(formEntity.entityType) {
			// CSS: can child div's inherit parent div's height
			case EntityTypes.TEXT_INPUT:
			case EntityTypes.SELECT_INPUT:
			case EntityTypes.CHECKBOX_INPUT:
			case EntityTypes.TEXTAREA_INPUT:
			case EntityTypes.DATETIME_INPUT:
				return this.generateFormInputPreview(formEntity);

			case EntityTypes.FORM_SECTION:
				return (
					<div 
						className={'formSection ' + 'span-' + formEntity.width}>
						{formEntity.childEntities.toJS().map(this.generateDragPreviewChildren)}
					</div>
				);

			case EntityTypes.PROMPT:
				return (
					<div
					className={'prompt ' + 'span-' + formEntity.width}>
					<TextAreaAutoResize
						style={{
							width:'100%',
							boxSizing: 'border-box'}}/> 
					</div>
				);

			case 'Filler':
				return (
					<div
						className={'filler ' + 'span-' + formEntity.width}/>
				);

			case EntityTypes.TEXTBLOCK:
				let content;
				if (formEntity.qxqTextBlock) {
					content = 
						<TextAreaResizable
							type={'textarea'}
							width={formEntity.width}
							rows={formEntity.qxqHeight}
							rowHeight={35}
							backgroundColor={Constants.BACKGROUND_COLORS.VALUES[formEntity.backgroundColor]}/>

				} else {
					let contentStyle = {
						width: BlueprintWidths[formEntity.width], 
						boxSizing: 'border-box', 
						marginBottom: '12px', 
						backgroundColor: Constants.BACKGROUND_COLORS.VALUES[formEntity.backgroundColor]
					};

					content = <TextAreaAutoResize
								text={formEntity.content}
								backgroundColor={Constants.BACKGROUND_COLORS.VALUES[formEntity.backgroundColor]}/>
				}
				return (
					<div className={'textBlock ' + 'span-' + formEntity.width}>
						{content}
					</div>
				);

			case EntityTypes.IMAGEBLOCK:
			case EntityTypes.HIGHLIGHT_IMAGEBLOCK:
				let img = null;
				if (formEntity.imgUrl) {
					img = <img
							src={formEntity.imgUrl}
							className={'span-' + formEntity.width}/>
				} else {
					let imageStyle = {
						position: 'absolute',
						top: 0,
						left: 0,
						bottom: 0,
						right: 0
					} 
					img = <input 
							type='file'
							style={{width: '100%'}}/>
				}
				return (
					<div
						className={'imageBlock ' + 'span-' + formEntity.width}
						style={{position: 'relative'}}>
						{img}
					</div>
				);
			default:
				break;
		}

		return null;
	},

	componentWillReceiveProps: function(nextProps) {
		if (!this.props.isDragging && nextProps.isDragging) {
			this.setState({
				dragPreview: this.generateDragPreview(nextProps.item)
			});
		} else if (!nextProps.isDragging) {
			this.setState({
				dragPreview: null
			});
		}
	}, 

	render: function () {
		var item = this.props.item;
		var itemType = this.props.itemType;
		var isDragging = this.props.isDragging;

		var dragLayerStyle = {
			position:'fixed',
			left: 0,
			top: 0,
			opacity: 0,
			pointerEvents: 'none',
			zIndex: 100
		};
			
		if (!isDragging) {
			return null;
		}

		if (isDragging && this.props.currentOffset != null) {
			dragLayerStyle.left = this.props.currentOffset.x;
			dragLayerStyle.top = this.props.currentOffset.y;
			dragLayerStyle.opacity = 1;
		}

		return <div 
					style={dragLayerStyle}>
					{this.state.dragPreview}
				</div>
		}
});

module.exports = {
	DragLayer: DragLayer(collect)(CustomDragLayer)};