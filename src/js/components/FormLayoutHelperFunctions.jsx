var React = require('react');
var ReactDOM = require('react-dom');
var Immutable = require('immutable');
var _ = require('underscore');
var $ = require('jquery');

var FormDesignerActions = require('../actions/FormDesignerActions.js');
var AppDispatcher = require('../dispatcher/Dispatcher.js');

var FormEntityStore = require('../stores/FormEntityStore.js');
var HelperFunctions = require('../HelperFunctions.jsx');

var BlueprintWidths = require('../constants/blueprint-widths.js');
var Constants = require('../constants/Constants.js');
var EntityTypes = Constants.ENTITY_TYPES;
var BackgroundColors = Constants.BACKGROUND_COLORS;
const DnDTypes = Constants.DND_TYPES;
const OperatorTypes = Constants.OPERATOR_TYPES;

const dropFormEntity =  function(droppedFormEntity, targetFormWrapper, dropIdX, targetComponent, childEntities) {
	if (droppedFormEntity.entityType === EntityTypes.PROMPT) {
		return;
	}
	
	var newFormEntity = new Immutable.Map(
		{
			entityType: droppedFormEntity.entityType,
			width: droppedFormEntity.width,
			prepend: -1, 
			append: -1, 
			id: droppedFormEntity.id
		}
	);

	// Assign new ID to new FormEntity
	if (droppedFormEntity.id === -1) {
		newFormEntity = newFormEntity.set('id', FormEntityStore.getNewId());
	}

	// Preserve all child entities of dragged form section
	if (droppedFormEntity.entityType === EntityTypes.FORM_SECTION) {
		if (droppedFormEntity.id === -1) {		
			newFormEntity = newFormEntity.set('childEntities', 
				Immutable.List.of(filler.set('width', droppedFormEntity.width).set('id', FormEntityStore.getNewId())));
		} else {
			var formEntityPath = HelperFunctions.getFormEntityPathToId(droppedFormEntity.id, childEntities);
			newFormEntity = newFormEntity.set('childEntities', 
				childEntities.getIn(formEntityPath.push('childEntities')));
		}
	}

	var droppedFormEntityWidth = droppedFormEntity.width;
	if (droppedFormEntity.entityType === EntityTypes.TEXT_INPUT
		|| droppedFormEntity.entityType === EntityTypes.SELECT_INPUT
		|| droppedFormEntity.entityType === EntityTypes.CHECKBOX_INPUT
		|| droppedFormEntity.entityType === EntityTypes.TEXTAREA_INPUT
		|| droppedFormEntity.entityType === EntityTypes.DATETIME_INPUT) {
		droppedFormEntityWidth += droppedFormEntity.promptPreWidth + droppedFormEntity.promptPostWidth;
		newFormEntity = newFormEntity.set('promptPreText', droppedFormEntity.promptPreText);
		newFormEntity = newFormEntity.set('promptPreWidth', droppedFormEntity.promptPreWidth);
		newFormEntity = newFormEntity.set('promptPostText', droppedFormEntity.promptPostText);
		newFormEntity = newFormEntity.set('promptPostWidth', droppedFormEntity.promptPostWidth);
	}

	if (droppedFormEntity.entityType === EntityTypes.TEXT_INPUT) {
		let inputType = droppedFormEntity.inputType;
		let validators = droppedFormEntity.validators;
		let dependencies = droppedFormEntity.dependencies;
		if (droppedFormEntity.id === -1) {
			inputType = 'string';
			validators = Immutable.fromJS([]);
			dependencies = Immutable.fromJS([{operator: OperatorTypes.AND, conditions: []}]);
		}
		newFormEntity = newFormEntity.set('inputType', inputType);
		newFormEntity = newFormEntity.set('validators', validators);
		newFormEntity = newFormEntity.set('dependencies', dependencies);
	} else if (droppedFormEntity.entityType === EntityTypes.SELECT_INPUT) {
		newFormEntity = newFormEntity.set('selectionOptions', droppedFormEntity.selectionOptions);

		let inputType = droppedFormEntity.inputType;
		let dependencies = droppedFormEntity.dependencies;
		if (droppedFormEntity.id === -1) {
			inputType = 'string';
			dependencies = Immutable.fromJS([{operator: OperatorTypes.AND, conditions: []}]);
		}
		newFormEntity = newFormEntity.set('inputType', inputType);	
		newFormEntity = newFormEntity.set('dependencies', dependencies);
	} else if (droppedFormEntity.entityType === EntityTypes.CHECKBOX_INPUT) {
		newFormEntity = newFormEntity.set('defaultChecked', droppedFormEntity.defaultChecked);

		let dependencies = droppedFormEntity.dependencies;
		if (droppedFormEntity.id === -1) {
			dependencies = Immutable.fromJS([{operator: OperatorTypes.AND, conditions: []}]);
		}
		newFormEntity = newFormEntity.set('dependencies', dependencies);
	} else if (droppedFormEntity.entityType === EntityTypes.TEXTAREA_INPUT) {
		newFormEntity = newFormEntity.set('rows', droppedFormEntity.rows);

		let dependencies = droppedFormEntity.dependencies;
		if (droppedFormEntity.id === -1) {
			dependencies = Immutable.fromJS([{operator: OperatorTypes.AND, conditions: []}]);
		}
		newFormEntity = newFormEntity.set('dependencies', dependencies);
	} else if (droppedFormEntity.entityType === EntityTypes.DATETIME_INPUT) {
		let inputType = droppedFormEntity.inputType;
		let precisionOptions = droppedFormEntity.precisionOptions;
		let allDateComponentsRequired = droppedFormEntity.allDateComponentsRequired;
		let displayTimeZoneOptions = droppedFormEntity.displayTimeZoneOptions;
		let timeZone = droppedFormEntity.timeZone;
		let validators = droppedFormEntity.validators;
		let dependencies = droppedFormEntity.dependencies;
		if (droppedFormEntity.id === -1) {
			inputType = 'date';
			precisionOptions = Immutable.fromJS([
				{name: 'Year', selected: false},
				{name: 'Month', selected: false},
				{name: 'Day', selected: false},
				{name: 'Hour', selected: false},
				{name: 'Minute', selected: false},
				{name: 'Second', selected: false},
			]);
			allDateComponentsRequired = false;
			displayTimeZoneOptions = false;
			timeZone = "Unknown/Unknown";
			validators = Immutable.fromJS([]);
			dependencies = Immutable.fromJS([{operator: OperatorTypes.AND, conditions: []}]);
		}
		newFormEntity = newFormEntity.set('inputType', inputType);
		newFormEntity = newFormEntity.set('precisionOptions', precisionOptions);
		newFormEntity = newFormEntity.set('allDateComponentsRequired', allDateComponentsRequired);
		newFormEntity = newFormEntity.set('displayTimeZoneOptions', displayTimeZoneOptions);
		newFormEntity = newFormEntity.set('timeZone', timeZone);		
		newFormEntity = newFormEntity.set('validators', validators);
		newFormEntity = newFormEntity.set('dependencies', dependencies);
	} else if (droppedFormEntity.entityType === EntityTypes.TEXTBLOCK) {
		let content = droppedFormEntity.content;
		let backgroundColor = droppedFormEntity.backgroundColor;
		let qxqTextBlock = droppedFormEntity.qxqTextBlock;
		let qxqHeight = droppedFormEntity.qxqHeight;
		if (droppedFormEntity.id === -1) {
			content = '';
			backgroundColor = BackgroundColors.NONE;
			qxqTextBlock = false;
			qxqHeight = 1;
		}
		newFormEntity = newFormEntity.set('content', content);
		newFormEntity = newFormEntity.set('backgroundColor', backgroundColor);
		newFormEntity = newFormEntity.set('qxqTextBlock', qxqTextBlock);
		newFormEntity = newFormEntity.set('qxqHeight', qxqHeight);
	} else if (droppedFormEntity.entityType === EntityTypes.IMAGEBLOCK) {
		newFormEntity = newFormEntity.set('altText', droppedFormEntity.altText);
		newFormEntity = newFormEntity.set('title', droppedFormEntity.title);

		if (droppedFormEntity.fromLocal && droppedFormEntity.id === -1) {
			// Read in the new file 
			var reader = new FileReader();
			reader.onload = (function(id) {
				return function(e) {
					window.setTimeout(FormDesignerActions.updateFormEntity(id, {imgUrl: e.target.result}), 30000);

					var x = new Image();
					x.src = e.target.result;
					x.onload = (function(id) {
						return function(e) {
							FormDesignerActions.updateFormEntity(id, {aspectRatio: e.currentTarget.naturalWidth / e.currentTarget.naturalHeight});
						}
					})(id)
				}
			})(newFormEntity.get('id'));
			reader.readAsDataURL(droppedFormEntity.file);
		} else {
			newFormEntity = newFormEntity.set('imgUrl', droppedFormEntity.imgUrl);
			newFormEntity = newFormEntity.set('aspectRatio', droppedFormEntity.aspectRatio);
		}
	} else if (droppedFormEntity.entityType === EntityTypes.HIGHLIGHT_IMAGEBLOCK) {
		newFormEntity = newFormEntity.set('altText', droppedFormEntity.altText);
		newFormEntity = newFormEntity.set('title', droppedFormEntity.title);
		newFormEntity = newFormEntity.set('imgUrl', droppedFormEntity.imgUrl);
		newFormEntity = newFormEntity.set('aspectRatio', droppedFormEntity.aspectRatio);
		newFormEntity = newFormEntity.set('selectedRegion', -1);
		newFormEntity = newFormEntity.set('hiddenRegions', droppedFormEntity.hiddenRegions);
		newFormEntity = newFormEntity.set('regions', droppedFormEntity.regions);
	}

	// Drop entity into it's own wrapper
	if (droppedFormEntity.id === targetFormWrapper.id) {
		var prepends = Math.floor((dropIdX - targetComponent.left) / Constants.PREPEND_WIDTH);
		var appends = (targetFormWrapper.prepend + targetFormWrapper.append) - prepends;
		var formEntityPath = HelperFunctions.getFormEntityPathToId(targetFormWrapper.id, childEntities);

		var validDrop = false;
		if (prepends >= 0 && appends >= 0) {
			validDrop = true;
		} else if (appends < 0) {
			/* Border Control Case 1: It's own wrapper
				Checking Entity on RIGHT to see if it has prepend to 'absorb' the overflow 
			*/
			if (!childEntities.getIn(formEntityPath.push('last')) 
				&& childEntities.getIn(formEntityPath.pop().push(formEntityPath.last()+1).push('prepend')) >= appends*-1) {
				validDrop = true;
			}
		}

		if (validDrop) {
			newFormEntity = newFormEntity.set('prepend', prepends);
			newFormEntity = newFormEntity.set('append', appends);
			if (!childEntities.getIn(formEntityPath).get('last')) {
				newFormEntity = newFormEntity.set('append', 0);
				childEntities = childEntities.updateIn(formEntityPath.pop(), list => list.delete(formEntityPath.last()));
				childEntities = childEntities.updateIn(formEntityPath.pop(), list => list.insert(formEntityPath.last(), newFormEntity));
				childEntities = childEntities.updateIn(formEntityPath.pop().push(formEntityPath.last()+1), 
					formEntity => formEntity.set('prepend', formEntity.get('prepend') + appends));
			} else {
				newFormEntity = newFormEntity.set('last', true);
				childEntities = childEntities.updateIn(formEntityPath.pop(), list => list.delete(formEntityPath.last()));
				childEntities = childEntities.updateIn(formEntityPath.pop(), list => list.insert(formEntityPath.last(), newFormEntity));
			}
		}

	} else if (targetFormWrapper.filler) {
		var prepends = Math.floor((dropIdX - targetComponent.left) / Constants.PREPEND_WIDTH);
		var appends = targetFormWrapper.width - (prepends + droppedFormEntityWidth);
		if (prepends >= 0 && appends >= 0) {
			newFormEntity = newFormEntity.set('prepend', prepends);
			newFormEntity = newFormEntity.set('append', appends);
			if (droppedFormEntity.id != -1) {
				childEntities = removeFormEntity(droppedFormEntity.id, childEntities);
			}
			childEntities = addFormEntity(Constants.FILLER, targetFormWrapper.id, newFormEntity, childEntities);
		}
	// Drop into a ('last' Form Entity) append
	} else if (dropIdX >= (targetComponent.right - targetFormWrapper.append * Constants.APPEND_WIDTH)) {
		var prepends = Math.floor((dropIdX - (targetComponent.right - targetFormWrapper.append * Constants.APPEND_WIDTH)) / Constants.PREPEND_WIDTH);
		var appends = targetFormWrapper.append - (prepends + droppedFormEntityWidth);
		if (prepends >= 0 && appends >= 0) {
			newFormEntity = newFormEntity.set('prepend', prepends);
			newFormEntity = newFormEntity.set('append', appends);
			if (droppedFormEntity.id != -1) {
				childEntities = removeFormEntity(droppedFormEntity.id, childEntities);
			}
			childEntities = addFormEntity(Constants.APPEND, targetFormWrapper.id, newFormEntity, childEntities);
		}
	} else if (dropIdX < (targetComponent.left + targetFormWrapper.prepend * Constants.PREPEND_WIDTH)) {
		/* Border Control Case 2: Into the prepend of Form Entity directly right of it */
		let droppedFormEntityPath = HelperFunctions.getFormEntityPathToId(droppedFormEntity.id, childEntities);
		let targetFormWrapperPath = HelperFunctions.getFormEntityPathToId(targetFormWrapper.id, childEntities);
		const isAdjacent = droppedFormEntityPath.size === targetFormWrapperPath.size && droppedFormEntityPath.last() + 1 === targetFormWrapperPath.last();

		if (isAdjacent && !droppedFormEntity.last) {
			var prepends = droppedFormEntity.prepend + droppedFormEntity.append + droppedFormEntityWidth + (Math.floor((dropIdX - targetComponent.left) / Constants.PREPEND_WIDTH)); // hanging off
			var appends = targetFormWrapper.prepend - ((Math.floor((dropIdX - targetComponent.left) / Constants.PREPEND_WIDTH)) + droppedFormEntityWidth);
			if (prepends >= 0 && appends >= 0) {
				newFormEntity = newFormEntity.set('prepend', prepends);
				newFormEntity = newFormEntity.set('append', appends);
				if (droppedFormEntity.id != -1) {
					childEntities = removeFormEntity(droppedFormEntity.id, childEntities);
				}
				childEntities = addFormEntity(Constants.PREPEND, targetFormWrapper.id, newFormEntity, childEntities);
			}
		} else {
			var prepends = Math.floor((dropIdX - targetComponent.left) / Constants.PREPEND_WIDTH);
			var appends = targetFormWrapper.prepend - (prepends + droppedFormEntityWidth);
			if (prepends >= 0 && appends >= 0) {
				newFormEntity = newFormEntity.set('prepend', prepends);
				newFormEntity = newFormEntity.set('append', appends);
				if (droppedFormEntity.id != -1) {
					childEntities = removeFormEntity(droppedFormEntity.id, childEntities);
				}
				childEntities = addFormEntity(Constants.PREPEND, targetFormWrapper.id, newFormEntity, childEntities);
			}
		}
	}
	// if (hasEnoughHeight === undefined) {
	// 	hasEnoughHeight = this.hasEnoughHeight(targetFormWrapper.id, droppedFormEntity.entityType !== 'FormSection' ? droppedFormEntity.height : (droppedFormEntity.height * 36));
	// }
	// if (hasEnoughHeight) {
		var formEntityPath = HelperFunctions.getFormEntityPathToId(targetFormWrapper.id, childEntities);
		childEntities = cleanUpTrailingFillers(childEntities, formEntityPath);
		//FormDesignerActions.updateFormSection(this.props.entity.get('id'), childEntities);
		FormDesignerActions.updateFormDesigner(childEntities);
		FormDesignerActions.setSelectedEntity(newFormEntity.get('id'));
	// 
};

const cleanUpTrailingFillers = function(childEntities, formEntityPath) {
	/* Not completed yet */
	var deleteIndex = childEntities.getIn(formEntityPath.pop()).size - 2; // -2 to get to the second to last row Index, since -1 will be the last filler
	while (deleteIndex >= 0 && childEntities.getIn(formEntityPath.pop().push(deleteIndex)).get('filler')) {
		childEntities = childEntities.updateIn(formEntityPath.pop(), list => list.delete(deleteIndex));
		deleteIndex--;
	}
	return childEntities;
};

const filler = new Immutable.Map({filler: true, height: 1, entityType: 'Filler'});

const addFormEntity = function(paddingType, paddingId, newFormEntity, formEntities) {
	// Make formEntityPath Immutable b/c we need to push / pop things to the path to manipulate certain Form Entities
	// Use formEntityPath.last() to get Index (within a childEntities array) of the target Form Entity 
	// Use formEntityPath.pop() to get the path to the childnEntities array (which contains the target Form Entity)

	var formEntityPath = HelperFunctions.getFormEntityPathToId(paddingId, formEntities);
	var widthOfParent = formEntities.getIn(formEntityPath).get('width');
	switch (paddingType) {
		case Constants.PREPEND:
			formEntities = formEntities.setIn(formEntityPath.push('prepend'), newFormEntity.get('append'));
			newFormEntity = newFormEntity.set('append', 0);
			formEntities = formEntities.updateIn(formEntityPath.pop(), list => list.insert(formEntityPath.last(), newFormEntity));
			break;
		case Constants.APPEND:
			formEntities = formEntities.setIn(formEntityPath.push('append'), 0);
			if (formEntities.getIn(formEntityPath.push('last'))) {
				newFormEntity = newFormEntity.set('last', true);
				formEntities = formEntities.setIn(formEntityPath.push('last'), false);
			} 
			formEntities = formEntities.updateIn(formEntityPath.pop(), list => list.insert(formEntityPath.last()+1, newFormEntity));
			break;
		case Constants.FILLER:
			newFormEntity = newFormEntity.set('last', true);
			formEntities = formEntities.updateIn(formEntityPath.pop(), list => list.delete(formEntityPath.last()));
			formEntities = formEntities.updateIn(formEntityPath.pop(), list => list.insert(formEntityPath.last(), newFormEntity));
			// If the filler that was dropped into is the last row of it's parent FormSection, then add a new filler row
			// console.log(newFormEntity.get('id'));
			// console.log(formEntities.getIn(formEntityPath.pop()).last().get('id'))
			if (newFormEntity.get('id') == formEntities.getIn(formEntityPath.pop()).last().get('id')) {
				formEntities = formEntities.updateIn(formEntityPath.pop(), 
					list => list.push(filler.set('id', FormEntityStore.getNewId()).set('width', widthOfParent)));
			}
			break;
	}
	return formEntities;
};

const removeFormEntity = function(id, formEntities, onlyDelete) {
	var formEntityPath = HelperFunctions.getFormEntityPathToId(id, formEntities);

	if (formEntities.getIn(formEntityPath).get('root')) {
		formEntities = formEntities.delete(formEntities.findIndex((entity) => entity.get('id') === id));
	} else {
		var widthOfParent = formEntities.getIn(formEntityPath.pop().pop()).get('width'); // formEntityPath.size > 1 this.props.entity.get('width');
		var fillerGotAdded = false;

		if (formEntities.getIn(formEntityPath).get('last') 
			&&	(formEntityPath.last()-1 < 0 // Single Form Entity in row, in the first row
				|| formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()-1)).get('last') 
				|| formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()-1)).get('filler'))) {
			// Remove FormEntity Case 1: Only FormEntity in pop()
			formEntities = formEntities.updateIn(formEntityPath.pop(), 
				list => list.delete(formEntityPath.last()));
			formEntities = formEntities.updateIn(formEntityPath.pop(),
				list => list.insert(formEntityPath.last(), filler.set('id', FormEntityStore.getNewId()).set('width', widthOfParent)));
			fillerGotAdded = true;
		} else if (formEntities.getIn(formEntityPath).get('last')) {
			// Remove FormEntity Case 2: Last (and not the only) FormEntity
			formEntities = formEntities.setIn(formEntityPath.pop().push(formEntityPath.last()-1).push('append'), 
				formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()-1).push('append')) 
				+ formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()).push('prepend'))
				+ formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()).push('append'))
				+ formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()).push('width'))
				+ ((formEntities.getIn(formEntityPath.push('entityType')) === EntityTypes.TEXT_INPUT
					|| formEntities.getIn(formEntityPath.push('entityType')) === EntityTypes.SELECT_INPUT
					|| formEntities.getIn(formEntityPath.push('entityType')) === EntityTypes.CHECKBOX_INPUT
					|| formEntities.getIn(formEntityPath.push('entityType')) === EntityTypes.TEXTAREA_INPUT
					|| formEntities.getIn(formEntityPath.push('entityType')) === EntityTypes.DATETIME_INPUT) ? 
					(formEntities.getIn(formEntityPath.push('promptPreWidth'))
					+ formEntities.getIn(formEntityPath.push('promptPostWidth'))) : 0));
			formEntities = formEntities.setIn(formEntityPath.pop().push(formEntityPath.last()-1).push('last'), true);
		} else if (!formEntities.getIn(formEntityPath).get('last')) {
			// Remove FormEntity Case 3: General Case
			formEntities = formEntities.setIn(formEntityPath.pop().push(formEntityPath.last()+1).push('prepend'), 
				formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()+1).push('prepend')) 
				+ formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()).push('prepend'))
				+ formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()).push('append'))
				+ formEntities.getIn(formEntityPath.pop().push(formEntityPath.last()).push('width'))
				+ ((formEntities.getIn(formEntityPath.push('entityType')) === EntityTypes.TEXT_INPUT
					|| formEntities.getIn(formEntityPath.push('entityType')) === EntityTypes.SELECT_INPUT
					|| formEntities.getIn(formEntityPath.push('entityType')) === EntityTypes.CHECKBOX_INPUT
					|| formEntities.getIn(formEntityPath.push('entityType')) === EntityTypes.TEXTAREA_INPUT
					|| formEntities.getIn(formEntityPath.push('entityType')) === EntityTypes.DATETIME_INPUT) ? 
					(formEntities.getIn(formEntityPath.push('promptPreWidth'))
					+ formEntities.getIn(formEntityPath.push('promptPostWidth'))) : 0));
		}
		if (!fillerGotAdded) {
			formEntities = formEntities.updateIn(formEntityPath.pop(), list => list.delete(formEntityPath.last()));
		}
	}

	if (onlyDelete) {
		FormDesignerActions.updateFormDesigner(formEntities);
		FormDesignerActions.removeDependenciesWithId(id);
	}
	return formEntities;
};

module.exports = {
	dropFormEntity: dropFormEntity,
	removeFormEntity: removeFormEntity
}