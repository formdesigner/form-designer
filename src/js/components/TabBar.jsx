var React = require('react');
var ReactDOM = require('react-dom');
var ReactDnD = require('react-dnd');
var FormDesignerActions = require('../actions/FormDesignerActions.js');
var $ = require('jquery');

var AutosizeInput = require('react-input-autosize');

var Constants = require('../constants/Constants.js');
var DnDTypes = Constants.DND_TYPES;

var TabBar = React.createClass({

	shouldComponentUpdate: function(nextProps, nextState) {
		return this.props.tabs.length != nextProps.length || 
			this.props.tabs.reduce((prev, currentTab, index) => (currentTab.name !== nextProps.tabs[index].name || currentTab.id != nextProps.tabs[index].id) || prev, false);
	},

	switchTabs: function(e) {
		this.props.switchTabs($('.tab').index(e.currentTarget));
	},

	onClickAdd: function(e) {
		FormDesignerActions.addTab();
	},

	updateTabName: function(id, value) {
		FormDesignerActions.updateFormEntity(id, {name: value});
	},

	render: function() {
		const styleTabBar = {
			background: 'rgba(128,128,128,.5)',
			height: '38px',
			float: 'none'
		};

		const styleAddButton = {
			backgroundImage: 'url(statics/plus-button.jpeg)', 
			backgroundSize: 'contain',
			backgroundRepeat: 'no-repeat', 
			height: '15px',
			float: 'left'
		};

		const connectDropTarget = this.props.connectDropTarget;

		return connectDropTarget(
			<div
				style={styleTabBar}>
					{this.props.tabs.map((tab, key) => {
						let active = this.props.activeIndex === key;

						return (
							<TabDnD
								id={tab.id}
								key={tab.id}
								index={key}
								name={tab.name}
								switchTabs={this.switchTabs}
								tabs={this.props.tabs}
								switchTabsOnDelete={this.props.switchTabs}
								deleteTab={this.props.deleteTab}
								reorderTabs={this.props.reorderTabs}
								updateTabName={this.updateTabName}
								active={active}
								appState={this.props.appState}/>
						);
					})}

					<button
						style={styleAddButton}
						onClick={this.onClickAdd}
						disabled={this.props.appState.get('addingAssociations')}/>
			</div>
		);
	}
});

const TabBarDropTargetSpec = {
	drop: function() {
		return {dropped: 'TabBar'};
	}
}

const TabBarDropTargetCollect = (connect, monitor) => {
	return {
		connectDropTarget: connect.dropTarget()
	}
}

var DropTarget = ReactDnD.DropTarget;
TabBar= DropTarget([DnDTypes.TEXT_BOX, DnDTypes.TAB], TabBarDropTargetSpec, TabBarDropTargetCollect)(TabBar);

var Tab = React.createClass({
	updateTabName: function(e) {
		this.props.updateTabName(this.props.id, e.target.value);
	},

	timeHovered: 0,

	hoverTimer: 0,

	render: function() {
		const {connectDragSource, connectDropTarget, active, isDragging, isOver} = this.props;

		const styleTab = {
			background: active ? 'green' : 'rgba(128,128,128,1)',
			height: '100%',
			lineHeight: '36px',
			border: '1px solid black',
			marginLeft: '2px',
			marginRight: '2px',
			paddingLeft: '15px',
			paddingRight: '15px',
			cursor: 'pointer',
			opacity: isDragging ? 0 : 1
		};

		if (isOver && this.timeHovered === 0) {
			this.hoverTimer = window.setInterval(() => this.timeHovered++, 100);
		} else {
			window.clearInterval(this.hoverTimer);
			this.timeHovered = 0;
		}	

		const styleInput = {
			background: active ? 'green' : 'rgba(128,128,128,1)',
			disabled: active ? 'false': 'true',
			border: '0px',
			cursor: active ? 'text' : 'pointer'
		};

		let addingAssocationsMode = false;

		if (this.props.appState.get('addingAssociations')) {
			addingAssocationsMode = true;
		}
		
		let addingAssociationsBlockStyle = {
			top: 0,
			left: 0,
			height: '100%',
			width: '100%',
			position: 'absolute'
		};

		let addingAssociationsLayover = 
			<div
				className={'addingAssociationsModeEntity'}
				style={addingAssociationsBlockStyle}
				onClick={this.props.switchTabs}/>;

		return connectDragSource(connectDropTarget(
			<div
				onClick={this.props.switchTabs}
				className={'tab'}
				style={{position:'relative', overflow: 'hidden'}}>
				<AutosizeInput
					style={styleTab}
					inputStyle={styleInput}
					value={this.props.name}
					onChange={this.updateTabName}/>

				{this.props.appState.get('addingAssociations') ? 
					addingAssociationsLayover : null}
			</div>
		));
	},
});

var tabDragSourceSpec = {
	beginDrag: function(props, monitor, component) {
		return {
			index: props.index,
			entityType: 'Tab',
		};
	},

	canDrag: function(props, monitor) {
		return !props.appState.get('addingAssociations');
	},

	endDrag: function(props, monitor, component) {
		if (!monitor.didDrop()) {
			if (props.tabs.length > 1) { //disallow deleting a lone tab
				props.deleteTab(props.index);
			}
		}
	}
};

var tabDragSourceCollect = function (connect, monitor) {
	return {
		connectDragSource: connect.dragSource(),
		isDragging: monitor.isDragging()
	};
};

var tabDropTargetSpec = {
	hover: (props, monitor, component) => {
		if (monitor.getItemType() !== DnDTypes.TAB) {
			if (!props.active) {
				if (component.decoratedComponentInstance.timeHovered > 10) {
					props.switchTabs({currentTarget: ReactDOM.findDOMNode(component)});
				}
			}
			return;
		}

		const dragIndex = monitor.getItem().index;
		const hoverIndex = props.index;

		if (dragIndex === hoverIndex) {
			return;
		}

	    // Determine rectangle on screen
	    const hoverBoundingRect = ReactDOM.findDOMNode(component).getBoundingClientRect();

	    // Get vertical middle
	    const hoverMiddleX = (hoverBoundingRect.right - hoverBoundingRect.left) / 2;

	    // Determine mouse position
	    const clientOffset = monitor.getClientOffset();

	    // Get pixels to the right
	    const hoverClientX = hoverBoundingRect.right - clientOffset.x;

	    // Only perform the move when the mouse has crossed half of the items width
	    // When dragging left, only move when the cursor is below 50%
	    // When dragging right, only move when the cursor is above 50%

	    // Dragging left
	    if (dragIndex > hoverIndex && hoverClientX < hoverMiddleX) {
	      return;
	    }

	    // Dragging right
	    if (dragIndex < hoverIndex && hoverClientX > hoverMiddleX) {
	      return;
	    }

	    props.reorderTabs(dragIndex, hoverIndex);
	    monitor.getItem().index = hoverIndex;	
	}
};

var tabDropTargetCollect = function (connect, monitor) {
	return {
		connectDropTarget: connect.dropTarget(),
		isOver: monitor.isOver()
	};
};

var DropTarget = ReactDnD.DropTarget;
var DragSource = ReactDnD.DragSource;
var TabDnD = DropTarget([DnDTypes.TAB, DnDTypes.PROMPT, DnDTypes.TEXT_BOX], tabDropTargetSpec, tabDropTargetCollect)
	(DragSource(DnDTypes.TAB, tabDragSourceSpec, tabDragSourceCollect)(Tab));

module.exports = {
	TabBar: TabBar
}