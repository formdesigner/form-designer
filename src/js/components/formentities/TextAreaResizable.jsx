var React = require('react');
var ReactDOM = require('react-dom');
var ResizableBox = require('react-resizable').ResizableBox;

var FormDesignerActions = require('../../actions/FormDesignerActions.js');
var AppStateStore = require('../../stores/AppStateStore.jsx');

var BlueprintWidths = require('../../constants/blueprint-widths.js');
var Constants = require('../../constants/Constants.js');

const TextAreaResizable = React.createClass({
	onResize: function(event, dimensions) {
		this.props.onResize(Math.floor(dimensions.size.height / this.props.rowHeight));
		this.forceUpdate()
	},

	onResizeStart: function() {
		FormDesignerActions.disableDrag();
	},

	onResizeStop: function() {
		FormDesignerActions.enableDrag();
	},

	onMouseEnter: function() { 
		FormDesignerActions.disableDrag();
	},

	onMouseLeave: function() {
		FormDesignerActions.enableDrag();
	},

	render: function() {

		const rowNumberDivStyle = {  
			position: 'absolute',
		    top: 0,
		    left: '-9px',
		    zIndex: '0'
		}

		const rowNumberStyle = {
			height: this.props.rowHeight,
			width: '8px',
			fontSize: '25%'
		}


		let rows = this.props.rows;
		let rowNumbers = [];
		for (var i = 0; i < rows; i++ ) {
			rowNumbers.push(<div style={rowNumberStyle}>{i+1}</div>);
		}

		let textarea = <textarea
							style={{
								width: '100%',
								boxSizing: 'border-box',
								height: this.props.rows * this.props.rowHeight,
								padding: '0px',
								margin: '0px',
								borderWidth: '0px',
								position: 'relative',
								zIndex: 1,
								backgroundColor: this.props.backgroundColor
							}}
							onMouseEnter={this.onMouseEnter}
							onMouseLeave={this.onMouseLeave} 
							height={this.props.rows * this.props.rowHeight}
							disabled={this.props.type === 'qxqTextBlock' || this.props.type === 'textAreaInput' ? true : false}/>
		
		return (
			<div
				className={'textAreaResizable'}>
				<ResizableBox
					width={BlueprintWidths[this.props.width]}
					height={this.props.rows * this.props.rowHeight}
					minConstraints={[Constants.MIN_ENTITY_WIDTH, this.props.rowHeight]}
					draggableOpts={{grid: [Constants.ENTITY_GAP, this.props.rowHeight]}}				
					onResize={this.onResize}
					onResizeStart={this.onResizeStart}
					onResizeStop={this.onResizeStop}>
						{textarea}
						<div
							style={rowNumberDivStyle}>
							{rowNumbers}
						</div>
				</ResizableBox>
			</div>
		)
	}
});

module.exports = {
	TextAreaResizable: TextAreaResizable
}