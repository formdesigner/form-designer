var React = require('react');
var ReactDOM = require('react-dom');
var _ = require('underscore');

var FormDesignerActions = require('../../actions/FormDesignerActions.js');
var FormEntityStore = require('../../stores/FormEntityStore.js');
var AppStateStore = require('../../stores/AppStateStore.jsx');
var AppDispatcher = require('../../dispatcher/Dispatcher.js');
var ResizableBox = require('react-resizable').ResizableBox;
var TextAreaAutoResize = require('./TextAreaAutoResize.jsx').TextAreaAutoResize;
var ColorPicker = require("react-simple-colorpicker");

var BlueprintWidths = require('../../constants/blueprint-widths.js');
var Constants = require('../../constants/Constants.js');
var EntityTypes = Constants.ENTITY_TYPES;
var DnDTypes = Constants.DND_TYPES;


let FormEntityHOC = require('./FormEntityHOC.jsx').FormEntityHOC;



const HighlightImageBlock = React.createClass({


	 // The easiest way is to imagine a line going straight up form the point. 
	 // If this line crosses an odd number of polygon borders, your point is inside the polygon. 
	 // (It would just cross one polygon border for a simple convex polygon)
	isInPolygon: function(point, polygon) {
		let edgesIntersectedOnTop = 0;
		let {testX, testY} = point;
		let vertices = polygon.vertices;

		for (var i = 0; i < vertices.length-1; i++) {
			// y - y1 = m (x - x1)
			let slope = (vertices[i].y - vertices[i+1].y) / (vertices[i].x - vertices[i+1].x);
			let intersectY = slope * (testX - vertices[i].x) + vertices[i].y;
			let leftBound = vertices[i].x <= vertices[i+1].x ? vertices[i].x : vertices[i+1].x;
			let rightBound = vertices[i].x > vertices[i+1].x ? vertices[i].x : vertices[i+1].x;
			if (intersectY < testY && testX >= leftBound && testX <= rightBound) {
				edgesIntersectedOnTop++;
			} 
		}

		return edgesIntersectedOnTop % 2;
	},

	canvas: null, context: null,

	componentDidMount: function() {
    	if (this.props.entityType === EntityTypes.HIGHLIGHT_IMAGEBLOCK) {
			let regions = this.props.entity.get('regions').toJS();

			let canvas = document.getElementById('imageBlockCanvas');
			let context = canvas.getContext('2d');
			context.clearRect(0, 0, canvas.width, canvas.height);

			for (var i = 0; i < regions.length; i++) {
				if (this.props.entity.get('hiddenRegions').toJS().indexOf(i) > -1) {
					continue; // Skip rendering invisible regions
				}

				let region = regions[i];
				let vertices = region.vertices;

				context.beginPath();
				context.moveTo(vertices[0].x, vertices[0].y);
				for (var j = 0; j < vertices.length; j++) {
					context.lineTo(vertices[j].x, vertices[j].y);
				}
				context.closePath();
				context.fillStyle = region.color.substr(0,region.color.length-2) + '.7)';
				context.fill();
			}
    	}
	},

	// Just change opacity of the selected element
	componentDidUpdate: function() {
		let regions = this.props.entity.get('regions').toJS();
		let canvas = document.getElementById('imageBlockCanvas');
		let context = canvas.getContext('2d');
		context.clearRect(0, 0, canvas.width, canvas.height);
		
		for (var i = 0; i < regions.length; i++) {
			if (this.props.entity.get('hiddenRegions').toJS().indexOf(i) > -1) {
				continue; // Skip rendering invisible regions
			}

			let region = regions[i];
			let vertices = region.vertices;

			context.beginPath();
			context.moveTo(vertices[0].x, vertices[0].y);
			for (var j = 0; j < vertices.length; j++) {
				context.lineTo(vertices[j].x, vertices[j].y);
			}
			// Making color opaque
			context.fillStyle = region.color.substr(0,region.color.length-2) + '.7)';
			
			context.lineWidth = 1;
			context.strokeStyle = region.color;
			if (i === this.props.appState.get('selectedRegionId')) {
				context.lineWidth = 5;
				context.strokeStyle = 'green';
			}
			context.stroke();
			context.fill();
			context.closePath();
		}
	},

	selectRegion: function(e) {
		e.stopPropagation();
		FormDesignerActions.setSelectedEntity(this.props.entity.get('id'));

		let offsetX = e.clientX - e.currentTarget.getBoundingClientRect().left;
		let offsetY = e.clientY - e.currentTarget.getBoundingClientRect().top;
		let point = {
			testX: offsetX,
			testY: offsetY
		};

		let regions = this.props.entity.get('regions').toJS();
		for (var i = 0; i < regions.length; i++) {
			if (this.props.entity.get('hiddenRegions').toJS().indexOf(i) > -1) {
				continue; // Skip rendering invisible regions
			}

			if (this.isInPolygon(point, regions[i])) {
				FormDesignerActions.setSelectedRegion(i);
				return;
			}
		}
		FormDesignerActions.setSelectedRegion(-1);
	},

	changeColor: function(color) {
		FormDesignerActions.updateFormEntity(this.props.entity.get('id'), 
			{
				regions: this.props.entity.get('regions').setIn([this.props.appState.get('selectedRegionId'), 'color'], color)
			}
		);
	},

	render: function() {

		const entity = this.props.entity;
		let id = entity.get('id');
		let width = entity.get('width');
		let entityType = entity.get('entityType');
		let imgUrl = entity.get('imgUrl');

		let img  = <img 
						width={BlueprintWidths[width]} 
						src={imgUrl}
						style={{marginBottom: 5}}/>
		
		let canvas = <canvas
						id='imageBlockCanvas'
						style={{position: 'absolute',
								top:0, 
								left:0}}
						width={BlueprintWidths[width]}
						height={BlueprintWidths[width] / this.props.entity.get('aspectRatio')}
						onClick={this.selectRegion}/>

		let colorPicker = null;
		let colorPickerStyle = {
			position: 'absolute',
			top: '100%',
			width: '10em',
			height: '13em',
			backgroundColor: '#fff',
			border: '1px solid rgba(0, 0, 0, 0.2)',
			borderRadius: '0.25em',
			boxShadow: '0 3px 6px rgba(0, 0, 0, 0.3)',
			zIndex: 1
		};

		if (this.props.appState.get('selectedRegionId') > -1 
			&& this.props.appState.get('selectedEntityId') === this.props.entity.get('id')
			&& !this.props.appState.get('addingAssociations')) {
			colorPicker = 
						<div 
							style={colorPickerStyle}
							onClick={(e) => {e.stopPropagation()}}>
								<ColorPicker 
									color={this.props.entity.get('regions').get(this.props.appState.get('selectedRegionId')).get('color')} 
									onChange={this.changeColor}/>
						</div>	
		}

		
		var minConstraints = [Constants.MIN_ENTITY_WIDTH, Constants.MIN_ENTITY_HEIGHT];
		var maxConstraints = [Constants.MAX_ENTITY_WIDTH, Constants.ROW_HEIGHT];
		
		return (
			<div>
				<ResizableBox
					width={BlueprintWidths[width]}
					minConstraints={minConstraints}
					maxConstraints={maxConstraints}
					draggableOpts={{grid: [Constants.ENTITY_GAP, Constants.ROW_HEIGHT]}}
					className={'highlightImageBlock' + ' span-' + width}
					onResize={this.props.onResize}
					onResizeStart={this.props.onResizeStart}
					onResizeStop={this.props.onResizeStop}>
					{img}
					{canvas}
				</ResizableBox>
				{colorPicker}
			</div>
		);
	}
});

module.exports = {
	HighlightImageBlock : FormEntityHOC(HighlightImageBlock)
}