var React = require('react');
var ReactDnD = require('react-dnd');
var ReactDOM = require('react-dom');
var $ = require('jquery');
var getEmptyImage = require('react-dnd-html5-backend').getEmptyImage;
var PureRenderMixin = require('react-addons-pure-render-mixin');

var FormDesignerActions = require('../../actions/FormDesignerActions.js');
var ResizableBox = require('react-resizable').ResizableBox;
var TextAreaAutoResize = require('./TextAreaAutoResize.jsx').TextAreaAutoResize;
var SelectPortal = require('./SelectDropdown.jsx').SelectPortal;
var Checkbox = require('./Checkbox.jsx').Checkbox;
var TextAreaResizable = require('./TextAreaResizable.jsx').TextAreaResizable;
let DateTime = require('./DateTime.jsx').DateTime;

var BlueprintWidths = require('../../constants/blueprint-widths.js');
var Constants = require('../../constants/Constants.js');
var EntityTypes = Constants.ENTITY_TYPES;
var DnDTypes = Constants.DND_TYPES;


let FormInput = React.createClass({
	mixins: [PureRenderMixin],

	render: function() {
		const entity = this.props.entity;
		let id = entity.get('id');
		let width = entity.get('width');
		let entityType = entity.get('entityType');

		// Form Entities cannot resize its height
		// The width is further constrained by parent FormSection's bounds
		// Bounded by promptPre and promptPost
		var maxConstraints = [Constants.MAX_ENTITY_WIDTH, Constants.ROW_HEIGHT];
		var hoveredArea;
		var hoveredProps = this.props.hoveredProps;
		if (hoveredProps) {
			if (hoveredProps.side == 'promptPre') {
				var hoveredAreaStyle = {
					width: BlueprintWidths[width] / 2,
					height: '100%',
					position: 'absolute',
					zIndex: 1,
					pointerEvents: 'none'
				}
			} else if (hoveredProps.side === 'promptPost') {
				var hoveredAreaStyle = {
					width: BlueprintWidths[width] / 2,
					height: '100%',
					left: '50%',
					position: 'absolute',
					zIndex: 1,
					pointerEvents: 'none'
				}
			}
			hoveredArea = <div
							style={hoveredAreaStyle}
							className={hoveredProps.valid ? 'valid' : 'invalid'}/>
		}
		
		var input;
		if (entityType === EntityTypes.TEXT_INPUT) {
			input = <TextAreaAutoResize
						id={id}
						type={'body'}/>;
		} else if (entityType === EntityTypes.TEXTAREA_INPUT) {
			input = <TextAreaResizable
						id={id}
						width={width}
						type={'textAreaInput'}
						rows={this.props.entity.get('rows')}
						onResize={(rows) => FormDesignerActions.updateFormEntity(id, {rows: rows})}
						rowHeight={13}/>
		} else if (entityType === EntityTypes.SELECT_INPUT) {
			input = <SelectPortal
						id={id}
						selectionOptions={this.props.entity.get('selectionOptions')}/>
		} else if (entityType === EntityTypes.CHECKBOX_INPUT) {
			input = <Checkbox 
						id={id}
						defaultChecked={this.props.entity.get('defaultChecked')}/>
		} else if (entityType === EntityTypes.DATETIME_INPUT) {
			input = <DateTime/>;
		}

		var resizableBox = <ResizableBox
								width={BlueprintWidths[width]}
								minConstraints={[Constants.MIN_ENTITY_WIDTH, Constants.MIN_ENTITY_HEIGHT]}
								draggableOpts={{grid: [Constants.ENTITY_GAP, Constants.ROW_HEIGHT]}}
								className={'input' + ' span-' + width}
								onResize={this.props.onResize}
								onResizeStart={this.props.onResizeStart}
								onResizeStop={this.props.onResizeStop}
								maxConstraints={maxConstraints}>
								{input}
							</ResizableBox>;
		

		var className = this.props.className + ' ';
		if (entityType === EntityTypes.TEXT_INPUT) {
			className += 'textInput';
		} else if (entityType === EntityTypes.TEXTAREA_INPUT) {
			className += 'textareaInput';
		} else if (entityType === EntityTypes.SELECT_INPUT) {
			className += 'selectInput';
		} else if (entityType === EntityTypes.CHECKBOX_INPUT) {
			className += 'checkboxInput';
		} else if (entityType === EntityTypes.DATETIME_INPUT) {
			className += 'dateTimeInput';
		}

		var connectDropTarget = this.props.connectDropTarget;
		var styleFormEntity = {
			width: BlueprintWidths[width],
			float: 'left',
			position: 'relative'
		};

		if (this.props.entity.get('promptPostWidth') === 0) {
			styleFormEntity.marginRight = 0;
		}

		return connectDropTarget(
			<div
				id={'formEntity' + id}
				className={className + ' span-' + width}
				style={styleFormEntity}>
				{hoveredArea}
				{resizableBox}
			</div>
		);
	}
});

var formInputDropTargetSpec = {
	canDrop: function(props, monitor) {
		return true;
	}, 

	hover: function(props, monitor, component) {
		var rectangle = ReactDOM.findDOMNode(component).getBoundingClientRect();

		if (monitor.getItem().entityType != EntityTypes.PROMPT) {
			return;
		}
		
		if (monitor.getClientOffset().x < (rectangle.left + rectangle.right) / 2
			 && monitor.getClientOffset().x > rectangle.left) {
			// Left side, trying to add a promptPre
			// Need to validate if already have a promptPre and if there is enough space (Form Section's job)
			var valid = props.isValidDropPrompt(props.entity.get('id'), 'promptPre');
			props.onHover('promptPre', valid);
		} else if (monitor.getClientOffset().x >= (rectangle.left + rectangle.right) / 2
			&& monitor.getClientOffset().x < rectangle.right) {
			// Right side, trying to add promprPost
			var valid = props.isValidDropPrompt(props.entity.get('id'), 'promptPost');
			props.onHover('promptPost', valid);
		}
	},

	drop: function(props, monitor, component) {
		var rectangle = ReactDOM.findDOMNode(component).getBoundingClientRect();
		
		if (monitor.getClientOffset().x < (rectangle.left + rectangle.right) / 2
			 && monitor.getClientOffset().x > rectangle.left) {
			// Left side, trying to add a promptPre
			props.dropPrompt(props.entity.get('id'), 'promptPre');
		} else if (monitor.getClientOffset().x >= (rectangle.left + rectangle.right) / 2
			&& monitor.getClientOffset().x < rectangle.right) {
			// Right side, trying to add promprPost
			props.dropPrompt(props.entity.get('id'), 'promptPost');
		}
	}
};

var formInputDropTargetCollect = function(connect, monitor) {
	return {
		connectDropTarget: connect.dropTarget()
	}
};

var DropTarget = ReactDnD.DropTarget;
var FormInputDnD = DropTarget(DnDTypes.PROMPT, formInputDropTargetSpec, formInputDropTargetCollect)(FormInput);

module.exports = {
	FormInput: FormInputDnD
}