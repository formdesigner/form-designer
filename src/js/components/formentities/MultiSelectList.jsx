let React = require('react');
let Immutable = require('immutable');

var FormDesignerActions = require('../../actions/FormDesignerActions.js');

const MultiSelectList = React.createClass({
	propTypes: {
		id: React.PropTypes.number,
		onChange: React.PropTypes.func
	},

	onChange: function(optionIndex, selected) {
		FormDesignerActions.updateFormEntity(this.props.id, 
			{
				precisionOptions: this.props.optionsList.setIn([optionIndex, 'selected'], selected)
			});
	},

	render: function() {

		const listStyle = {
			listStyleType: 'none',
			marginBottom:'5px'
		};

		return (
			<div>
				<ul
					style={listStyle}>
					{this.props.optionsList.toJS().map((option, index) => 
						<SelectOption
							id={index}
							key={index}
							name={option.name}
							selected={option.selected}
							onChange={this.onChange}/>)}
				</ul>
			</div>
		);
	}
})

const SelectOption = React.createClass({
	propTypes: {
		id: React.PropTypes.number,
		name: React.PropTypes.string,
		selected: React.PropTypes.bool,
		onChange: React.PropTypes.func
	},

	onClick: function() {
		this.props.onChange(this.props.id, !this.props.selected)
	},

	render: function() {
		const optionStyle = {
			background: this.props.selected ? 'green' : 'red'
		};

		return (
			<li>
				<input
					type='checkbox'
					style={optionStyle}
					checked={this.props.selected}
					onChange={this.onClick}/>
				{this.props.name}
			</li>
		);
	}
});

module.exports = {
	MultiSelectList: MultiSelectList
}