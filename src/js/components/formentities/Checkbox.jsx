var React = require('react');
var ReactDnD = require('react-dnd');
var ReactDOM = require('react-dom');

var FormDesignerActions = require('../../actions/FormDesignerActions.js');

var Constants = require('../../constants/Constants.js');

var Checkbox = React.createClass({
	onClick: function(e) {
		FormDesignerActions.updateFormEntity(this.props.id, {defaultChecked: e.target.checked});
	},

	render: function() {
		const style = 
			{
				position: 'absolute',
				top: '50%',
				left: '50%',
				height: '10px',
				width:'10px',
				marginLeft:'-5px',
				marginTop: '-5px'
			}

		return (
			<input
				style={style}
				type='checkbox'
				onClick={this.onClick}
				checked={this.props.defaultChecked}
				readOnly={this.props.readOnly}/>
		);
	}
});

module.exports = {
	Checkbox: Checkbox
}
