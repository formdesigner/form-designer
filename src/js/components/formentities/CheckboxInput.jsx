var FormInputFactory = require('./FormInputFactory.jsx').FormInputFactory;
let FormEntityHOC = require('./FormEntityHOC.jsx').FormEntityHOC;
var Constants = require('../../constants/Constants.js');
var EntityTypes = Constants.ENTITY_TYPES;

module.exports = {
	CheckboxInput: FormEntityHOC(FormInputFactory(EntityTypes.CHECKBOX_INPUT))
}