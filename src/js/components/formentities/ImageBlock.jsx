var React = require('react');
var ReactDOM = require('react-dom');

var FormEntityStore = require('../../stores/FormEntityStore.js');
var AppStateStore = require('../../stores/AppStateStore.jsx');

var FormDesignerActions = require('../../actions/FormDesignerActions.js');
var ResizableBox = require('react-resizable').ResizableBox;
var TextAreaAutoResize = require('./TextAreaAutoResize.jsx').TextAreaAutoResize;

var BlueprintWidths = require('../../constants/blueprint-widths.js');
var Constants = require('../../constants/Constants.js');
var EntityTypes = Constants.ENTITY_TYPES;
var DnDTypes = Constants.DND_TYPES;

let FormEntityHOC = require('./FormEntityHOC.jsx').FormEntityHOC;

const ImageBlock = React.createClass({

	addFile: function(e) {
		var reader = new FileReader();
		reader.onload = (function(id) {
			return function(e) {
				FormDesignerActions.updateFormEntity(id, {imgUrl: e.target.result});
				var x = new Image();
				x.src = e.target.result;
				x.onload = (function(id) {
					return function(e) {
						FormDesignerActions.updateFormEntity(id, {aspectRatio: e.currentTarget.naturalWidth / e.currentTarget.naturalHeight})
					}
				})(id);
		}
		})(this.props.entity.get('id'));
		reader.readAsDataURL(e.target.files[0]);
	},

	render: function() {

		const entity = this.props.entity;
		let id = entity.get('id');
		let width = entity.get('width');
		let entityType = entity.get('entityType');
		let imgUrl = entity.get('imgUrl');

		let img = null;
		if (imgUrl) {
			img = <img 
					width={BlueprintWidths[width]} 
					src={imgUrl}
					style={{marginBottom: 5}}/>
		} else {
			img = <input 
					type='file' 
					onChange={this.addFile}
					style={{width: '100%'}}/>
		}

		var minConstraints = [Constants.MIN_ENTITY_WIDTH, Constants.MIN_ENTITY_HEIGHT];
		var maxConstraints = [Constants.MAX_ENTITY_WIDTH, Constants.ROW_HEIGHT];

		return (
			<div>
				<ResizableBox
					width={BlueprintWidths[width]}
					minConstraints={minConstraints}
					maxConstraints={maxConstraints}
					draggableOpts={{grid: [Constants.ENTITY_GAP, Constants.ROW_HEIGHT]}}
					className={'imageBlock' + ' span-' + width}
					onResize={this.props.onResize}
					onResizeStart={this.props.onResizeStart}
					onResizeStop={this.props.onResizeStop}>
					{img}
				</ResizableBox>
			</div>
		);
	}
});


module.exports = {
	ImageBlock : FormEntityHOC(ImageBlock)
}