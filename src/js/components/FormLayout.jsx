var React = require('react');
var ReactDnD = require('react-dnd');
var update = require('react-addons-update');
var PureRenderMixin = require('react-addons-pure-render-mixin');
var $ = require('jquery');
var Radium = require('radium');

var FormDesignerActions = require('../actions/FormDesignerActions.js');
var AppDispatcher = require('../dispatcher/Dispatcher.js');
var HelperFunctions = require('../HelperFunctions.jsx');
var FormLayoutHelperFunctions = require('./FormLayoutHelperFunctions.jsx');

var FormEntityStore = require('../stores/FormEntityStore.js');
var FormSection = require('./formentities/FormSection.jsx').FormSection;
var TabBar = require('./TabBar.jsx').TabBar;
var FormEntityAttributesBox = require('./FormEntityAttributesBox.jsx').FormEntityAttributesBox;

var BlueprintWidths = require('../constants/blueprint-widths.js');
var Constants = require('../constants/Constants.js');
var DnDTypes = Constants.DND_TYPES;
var EntityTypes = Constants.ENTITY_TYPES;

// //////////////////////////////////////////////////////////////////////////////
// FormLayout.
// Handle layout of Form Entities 		  
// //////////////////////////////////////////////////////////////////////////////

var collect = function(connect, monitor) {
	return {
		connectDropTarget: connect.dropTarget(),
		isOverInvalid: monitor.isOver({shallow:true})
	}
};

var spec = {
	canDrop: function(props, monitor, component) {
		return false;
	}
};

var ExportButton = React.createClass({
	render: function() {
		return (
			<div>
				<button 
					onClick={this.props.onClick}>
					Serialize
				</button>
			</div>
		)}
});

var JSONInputBox = React.createClass({
	onClick: function() {
		this.props.deserialize(this.refs.input.value);
	},

	render: function() {
		return (
			<div>
				<input
					ref={'input'}
					type='text'
					defaultValue='Enter JSON here'/>
				<button onClick={this.onClick}>
					Deserialize
				</button>
			</div>
		);
		
	}
});

let FormLayout = Radium(React.createClass({

	mixins: [PureRenderMixin],
	
	getInitialState: function() {
		return {
			activeIndex: 0
		};
	},

	// setNewHeight: function(formEntities) {
	// 	var newHeight = 1; // case where there are multiple filler rows at end
	// 	var maxInThatRow = 0;
	// 	formEntities.map(function(formEntity) {
	// 		var currentEntityHeight = formEntity['height'];
	// 		maxInThatRow = currentEntityHeight > maxInThatRow ? currentEntityHeight : maxInThatRow;
	// 		if (formEntity['last']) {
	// 			newHeight += maxInThatRow;
	// 			maxInThatRow = 0;
	// 		}
	// 	});
	// 	return newHeight;
	// },

	// calculateRowHeight: function(id, formEntities) {
	// 	var beforeId = id - 1;
	// 	var rowHeight = formEntities[id]['height'];
	// 	while (beforeId > -1 && !formEntities[beforeId]['last'] && !formEntities[beforeId]['filler']) {
	// 		var currentRowHeight = formEntities[beforeId]['height'];
	// 		rowHeight = currentRowHeight > rowHeight ? currentRowHeight : rowHeight;
	// 		beforeId--;
	// 	}
	// 	var afterId = id; // to account for if current FormEntity is a last
	// 	while (afterId < formEntities.length && !formEntities[afterId]['last'] && !formEntities[afterId]['filler']) {
	// 		var currentRowHeight = formEntities[afterId]['height'];
	// 		rowHeight = currentRowHeight > rowHeight ? currentRowHeight : rowHeight;
	// 		afterId++;
	// 	}
	// 	if (afterId < formEntities.length) {
	// 		rowHeight = formEntities[afterId]['height'] > rowHeight ? formEntities[afterId]['height'] : rowHeight;
	// 	}
	// 	return rowHeight;
	// },

	serialize: function() {
		FormDesignerActions.serialize();
	},

	deserialize: function(input) {
		FormDesignerActions.deserialize(input);
		this.forceUpdate();
	},

	switchTabs: function(index) {
		this.setState({activeIndex: index});
		if (!this.props.appState.get('addingAssociations')) {
			FormDesignerActions.setSelectedEntity(-1);
		}
	},

	reorderTabs: function(dragIndex, hoverIndex) {
		FormDesignerActions.reorderTabs(dragIndex, hoverIndex);
		if (dragIndex === this.state.activeIndex) {
			this.setState({activeIndex: hoverIndex});
		} else if (dragIndex < this.state.activeIndex
					&& hoverIndex >= this.state.activeIndex) {
			this.setState({activeIndex: this.state.activeIndex-1});
		} else if (dragIndex > this.state.activeIndex
					&& hoverIndex <= this.state.activeIndex) {
			this.setState({activeIndex: this.state.activeIndex+1});
		}
	},

	deleteTab: function(index) {
		if (index <= this.state.activeIndex && this.state.activeIndex > 0) {
			this.setState((previousState) => {return {activeIndex: previousState.activeIndex-1}});
		}
		this.deleteFormEntity(this.props.formEntities.get(index).get('id'));
	},

	dropFormEntity: function(droppedFormEntity, targetFormWrapper, dropIdX, targetComponent) {
		FormLayoutHelperFunctions.dropFormEntity(droppedFormEntity, targetFormWrapper, dropIdX, targetComponent, this.props.formEntities);
	},

	deleteFormEntity: function(id) {
		let formEntityPath = HelperFunctions.getFormEntityPathToId(id, this.props.formEntities);
		let entityToDelete = this.props.formEntities.getIn(formEntityPath);
		if (entityToDelete.get('entityType') === EntityTypes.FORM_SECTION
			&& (entityToDelete.get('id') === this.props.appState.get('selectedEntityId') ||
				entityToDelete.get('childEntities').find((entity) => entity.get('id') === this.state.selectedEntityId))) {
			FormDesignerActions.setSelectedEntity(-1);
		} else if (entityToDelete.get('id') === this.props.appState.get('selectedEntityId')) {
			FormDesignerActions.setSelectedEntity(-1);
		}

		FormLayoutHelperFunctions.removeFormEntity(id, this.props.formEntities, true);
	},

	render: function() {
		var rootFormSection = this.props.formEntities.get(this.state.activeIndex);
		let selectedEntity;
		if (this.props.appState.get('selectedEntityId') >= 0) {
			let formEntityPath = HelperFunctions.getFormEntityPathToId(this.props.appState.get('selectedEntityId'), this.props.formEntities);
			selectedEntity = this.props.formEntities.getIn(formEntityPath);
		}

		const connectDropTarget = this.props.connectDropTarget;
		return (
			<div
				style={[formLayoutStyles.formLayout]}>
				
				<TabBar
					tabs={this.props.formEntities.reduce(
						(currentlyReduced, currentRootFormSection) => 
							currentlyReduced.concat({name: currentRootFormSection.get('name'), 
													 id: currentRootFormSection.get('id')}), 
							[])}
					switchTabs={this.switchTabs}
					reorderTabs={this.reorderTabs}
					deleteTab={this.deleteTab}
					activeIndex={this.state.activeIndex}
					appState={this.props.appState}/>
				
				{connectDropTarget(
					<div 
						style={[formLayoutStyles.grid]}
					 	className='showgrid'
					 	id='showgrid'>
						<FormSection
							entity={rootFormSection}
							dropFormEntity={this.dropFormEntity}
							deleteFormEntity={this.deleteFormEntity}
							appState={this.props.appState}
							// Need this to trigger a rerender when associations are modified
							// Alternative is to make all FormEntities an entry point for Store
							// Here the entry point is at the top and just passed down as props
							/>
					</div>
				)}

				<FormEntityAttributesBox
					entity={selectedEntity}
					appState={this.props.appState}/>
			</div>
		)
	}
}));

let formLayoutStyles = {
	formLayout: {
		

		'@media (min-width: 1300px)': {

		},

		float: 'left'
	},

	tabBar: {

	},

	grid: {
		position: 'relative',
		border: '1px solid black',
		overflowY: 'scroll',
		overflowX: 'hidden',
		minHeight: '200px',

		'@media (max-width: 1205px)': {
			height: 'calc(100vh - 410px)'
		},

		'@media (min-width: 1206px) and (max-width: 1526px)': {
			height: 'calc(100vh - 335px)'
		},

		'@media (min-width: 1527px)' : {
			height: '100vh',
			float: 'left'
		}
	},

	formEntityAttributesBox: {

	}
	
}

// Catch the drop so that monitor.didDrop() will return true to prevent 
// Deletion of FormEntities. 
const FormLayoutDropTargetSpec = {
	drop: function() {
		return {dropped: 'FormLayout'};
	}
}

const FormLayoutDropTargetCollect = (connect, monitor) => {
	return {
		connectDropTarget: connect.dropTarget()
	}
}

var DropTarget = ReactDnD.DropTarget;
FormLayout = DropTarget([DnDTypes.TEXT_BOX, DnDTypes.TAB], FormLayoutDropTargetSpec, FormLayoutDropTargetCollect)(FormLayout);

module.exports = {
	FormLayout: FormLayout
}